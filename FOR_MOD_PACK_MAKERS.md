* If there are any features you'd like me to added just @ me in either fabicord

* .minecraft/dynocap_resources is the root for all configs
  * Prefabs(stored in the prefab folder) are serialized dynocaps that can be added to loot tables or obtained using the /rustycap command
  * Templates are independent dynocap items with their own personalized settings
  * Tweakers(in the tweakers folder) are a way to modifiy loottables.
* You can create prefabs using the rusty caps gui while it's filled
  * When they are saved they are stored in `.minecraft/config/DynocapSerialization`
* Don't try to manually edit prefabs, but nbtexplorer can probably do it.
* Template example - JSON format. Any file name. In `.minecraft/dynocap_resource/templates/`
```json
{
    "id":"custom_cap_identifier", // Lowercase. Optinally you can register it in your own namespace using the format `younamespace:custom_cap_identifier`; otherwise it will be registered under the `dynocap` namespace
    "attributes":[
        "damageable", // allows your cap to take damage on use
        "breakable", // allows cap to break when at max damage
                    // without this the cap will stop at 1 and be unuable until repaired
        "repairable", // can be repaired in anvil with specified repair_item. Must also be damageable
        "fixed_size", // Size will be locked at max_*
        "fixed_color", // color will be locked
        "3dmodel",
        "capturesEntities",
        "requiresEmptyArea",
        "usableInAdventureMode"
    ],
    "max_width":15, 
    "max_depth":15, 
    "max_height":7, 
    "default_width":5, // Optional Field
    "default_depth":5, // Optional Field
    "default_height":5, // Optional Field
    "color":"FF00FF", // Optional Field RGB format
    "max_damage":40, // Optional Field unless it has `damageable` attrib
    "default_name":"Custom Dynocap", // Optional Field
    "repair_item":"minecraft:iron_ingot", // Optional Field unless it has `repairable` attrib
    "model_color":"FFDDDDDD", // Optional Field. base color for 3dmodel,
    "preventCaptureBlocks":["minecraft:bedrock"], // Can be Empty
    "preventOverrideBlocks":["minecraft:bedrock"], // Can be Empty
    "preventCaptureEntities":["minecraft:villager"] // Can be Empty
}
```
* Tweaker example - JSON format. Any file name. In `.minecraft/dynocap_resource/tweakers/`
```json
{
    "target_table": "minecraft:entities/silverfish",
    "pool": [{
        "rolls": 1,
        "entries": [
            {
                "type": "minecraft:item",
                "name": "minecraft:stone"
            }
        ]
    }]
}
```
* Prefab entry type:
```json
{
    "type": "dynocaps:rustycap",
    "prefabname": "yourprefab.dynobin" // As seen in /rustycaps command
}
```