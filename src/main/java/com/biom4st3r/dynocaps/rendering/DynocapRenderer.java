package com.biom4st3r.dynocaps.rendering;

import java.util.HashMap;
import java.util.Random;
import java.util.Set;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.color.block.BlockColors;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.RenderLayers;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BlockEntityRenderDispatcher;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.entity.Entity;
import net.minecraft.util.Tickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockRenderView;

import net.fabricmc.fabric.api.client.render.fluid.v1.FluidRenderHandlerRegistry;
import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel;

import com.biom4st3r.dynocaps.api.BlockContainer2;
import com.biom4st3r.dynocaps.api.BlockInstance;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.mixin.rendering.BlockEntityAccessor;
import com.biom4st3r.dynocaps.util.ClientHelper;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import org.apache.commons.lang3.tuple.Triple;

public class DynocapRenderer {
    static Random random = new Random();

    @SuppressWarnings({"rawtypes"})
    public Set<Triple<BlockEntity, BlockEntityRenderer, BlockPos>> blockEntityCache = Sets.newHashSet();
    public Entity[][] entities;
    public static final IntSet PROHIBITED_TICKABLE = new IntOpenHashSet();
    static QuadFluidRenderer fluidRenderer = new QuadFluidRenderer();
    final float oneTick = (1f / 20f) * 1000;
    long last = System.currentTimeMillis();

    public void renderEntities(float delta, MatrixStack stack, VertexConsumerProvider immediate, int light) {
        for (Entity[] es : entities) {
            stack.push();
            stack.translate(es[0].getX(), es[0].getY(), es[0].getZ());
            for (Entity e : es) {
                EntityRenderer<? super Entity> renderer = MinecraftClient.getInstance().getEntityRenderDispatcher().getRenderer(e);
                renderer.render(e, e.getYaw(delta), delta, stack, immediate, light);
            }
            stack.pop();
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public void renderBlockEntities(float delta, MatrixStack stack, VertexConsumerProvider immediate, int light) {
        for (Triple<BlockEntity, BlockEntityRenderer, BlockPos> instance : blockEntityCache) {
            if (System.currentTimeMillis() > (long) (last + oneTick)) {
                last = System.currentTimeMillis();
                if (!PROHIBITED_TICKABLE.contains(instance.getLeft().getType().hashCode()) && instance.getLeft() instanceof Tickable) {
                    try {
                        ((Tickable) instance.getLeft()).tick();
                    } catch (Throwable t) {
                        PROHIBITED_TICKABLE.add(instance.getLeft().getType().hashCode());
                    }
                }
            }
            stack.push();
            stack.translate(instance.getRight().getX(), instance.getRight().getY(), instance.getRight().getZ());
            try {
                instance.getMiddle().render(instance.getLeft(), delta, stack, immediate, light, OverlayTexture.DEFAULT_UV);
            } catch (IllegalArgumentException e) {
                // do nothing
            }

            stack.pop();
        }
    }

    protected void handleVanillaBlock(AgnosticRenderContext cache, BlockInstance instance, BakedModel model, BlockRenderView view,
            Random random, boolean cull) {
        int color = colors.getColor(instance.state, view, instance.relativePos, 0);
        if (cull) cache.pushTransform((quad) -> {
            quad.spriteColor(0, color, color, color, color);
            Direction face = quad.lightFace();
            return quad.cullFace() == null || Block.shouldDrawSide(instance.state, view, instance.relativePos, face);
        });
        cache.fallbackConsumer.accept(model);
        if (cull) cache.popTransform();
    }

    protected void handleFabricBlock(AgnosticRenderContext cache, FabricBakedModel model, BlockInstance instance, BlockRenderView view,
            Random random) {
        int color = colors.getColor(instance.state, view, instance.relativePos, 0);
        cache.pushTransform((quad) -> {
            if (quad.colorIndex() == 0 && quad.spriteColor(0, 0) == -1) {
                quad.spriteColor(0, color, color, color, color);
            }
            return true;
        });
        model.emitBlockQuads(view, instance.state, instance.relativePos, () -> random, cache);
        cache.popTransform();
    }

    protected void handleFluidBlock(AgnosticRenderContext cache, BlockInstance instance, BlockRenderView view, boolean cull) {
        boolean shouldRender = fluidRenderer.render(view, instance.relativePos, instance.state.getFluidState());
        int fcolor = FluidRenderHandlerRegistry.INSTANCE.get(instance.state.getFluidState().getFluid()).getFluidColor(view, instance.relativePos.toImmutable(), instance.state.getFluidState());
        if (cull)
            cache.pushTransform((quad) -> {
                quad.spriteColor(0, fcolor, fcolor, fcolor, fcolor);
                return shouldRender;
            });
        cache.meshConsumer().accept(fluidRenderer.build());
        if (cull)
            cache.popTransform();
    }

    public AgnosticRenderContext fillQuadCache(IDynocapComponent component, AgnosticRenderContext cache) {
        return fillQuadCache(component.getView(), component.getContainer(), cache);
    }
    public AgnosticRenderContext fillQuadCache(IDynocapComponent component) {
        return fillQuadCache(component.getView(), component.getContainer(), component.getCache());
    }
    BlockColors colors = ClientHelper.client.getBlockColors();

    @SuppressWarnings({"rawtypes"})
    public AgnosticRenderContext fillQuadCache(BlockRenderView view, BlockContainer2 container, AgnosticRenderContext cache) {
        HashMap<BlockState, BakedModel> stateToModelCache = Maps.newHashMap();
        container.forEach((instance) -> {
            if (instance.state.isAir()) return;

            //saves about 500µseconds on dabergs prefab
            BakedModel iterModel = stateToModelCache.get(instance.state);
            if (iterModel == null) {
                stateToModelCache.put(instance.state, RenderHelper.getModel(instance.state));
                iterModel = stateToModelCache.get(instance.state);
            }

            // Position offset transform
            // Alpha handling
            cache.pushTransform((quadView) -> {
                Vector3f VICTIM = new Vector3f();
                for (int i = 0; i < 4; i++) {
                    int light = view.getLightLevel(null, instance.relativePos);
                    quadView.lightmap(i, RenderHelper.packLight(light, light));
                    quadView.copyPos(i, VICTIM);
                    VICTIM.add(instance.relativePos.getX(), instance.relativePos.getY(), instance.relativePos.getZ());
                    quadView.pos(i, VICTIM);
                    if (quadView.cullFace() != null && quadView.cullFace().ordinal() > 1) { // Direction is not up or down
                        int l0 = RenderHelper.packLight(light-3, light-3);
                        int l1 = RenderHelper.packLight(light-4, light-4);
                        quadView.lightmap(l0, l1, l0, l1);
                    }

                    int color = quadView.spriteColor(i, 0);
                    if ((color & 0xFF000000) >> 24 == 0) {
                        // Weird hax
                        quadView.spriteColor(i, 0, 0xFF000000 | color);
                    }
                }
                return true;
            });
            if (instance.isFluid()) {
                // Fapi implementation not handling tint/alpha isn't a problem, because hax in Primordial quad transform.
                cache.provideContext(RenderLayers.getFluidLayer(instance.state.getFluidState()), instance.state);
                handleFluidBlock(cache, instance, view, true);
            }
            if (instance.state.getRenderType() != BlockRenderType.INVISIBLE) {
                cache.provideContext(RenderLayers.getBlockLayer(instance.state), instance.state);
                if (((FabricBakedModel) iterModel).isVanillaAdapter()) handleVanillaBlock(cache, instance, iterModel, view, random, true);
                else handleFabricBlock(cache, (FabricBakedModel) iterModel, instance, view, random);
            }
            if (instance.isBlockEntity()) {
                BlockEntity be;
                BlockEntityRenderer renderer;
                try {
                    be = BlockEntity.createFromTag(instance.state, instance.entityTag);
                    renderer = BlockEntityRenderDispatcher.INSTANCE.get(be);
                } catch(Throwable t) {
                    t.printStackTrace();
                    renderer = null;
                    be = null;
                }
                 
                if (renderer != null) {
                    ((BlockEntityAccessor) be).setWorld(ClientHelper.world.get());
                    ((BlockEntityAccessor) be).setCachedState(instance.state);
                    this.blockEntityCache.add(Triple.of(be, renderer, instance.relativePos.toImmutable()));
                }
            }
            cache.popTransform();
        });
        return cache;
    }

    public void clear() {
        blockEntityCache.clear();
    }
}
// QuadTransform ONE_PIXEL_TRANSFROM = (view) -> {
//     float u = view.spriteU(0, 0);
//     float v = view.spriteV(0, 0);
//     for (int i = 0; i < 4; i++) {
//         view.sprite(i, 0, u, v);
//     }
//     return true;
// };

// QuadTransform ATLAS_TRANSFORM = (view) -> {
//     float u = 0.58F;
//     float v = 0.52F;
//     for (int i = 0; i < 4; i++) {
//         view.sprite(i, 0, u, v);
//     }
//     return true;
// };
