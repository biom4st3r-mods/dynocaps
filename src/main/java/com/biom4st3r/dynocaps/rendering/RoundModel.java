package com.biom4st3r.dynocaps.rendering;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import com.biom4st3r.dynocaps.register.BlockEnum;
import com.mojang.datafixers.util.Pair;

import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.renderer.v1.mesh.QuadEmitter;
import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.BakedQuad;
import net.minecraft.client.render.model.ModelBakeSettings;
import net.minecraft.client.render.model.ModelLoader;
import net.minecraft.client.render.model.UnbakedModel;
import net.minecraft.client.render.model.json.ModelOverrideList;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.render.model.json.Transformation;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.util.SpriteIdentifier;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockRenderView;

public class RoundModel implements BakedModel, FabricBakedModel, UnbakedModel {
    static {
        BlockRenderLayerMap.INSTANCE.putBlock(BlockEnum.DISPLAY.asBlock(), RenderLayer.getTranslucent());
    }

    @Override
    public Collection<Identifier> getModelDependencies() {
        return Collections.emptyList();
    }

    @Override
    public Collection<SpriteIdentifier> getTextureDependencies(Function<Identifier, UnbakedModel> unbakedModelGetter, Set<Pair<String, String>> unresolvedTextureReferences) {
        return Collections.emptyList();
    }

    @Override
    public BakedModel bake(ModelLoader loader, Function<SpriteIdentifier, Sprite> textureGetter, ModelBakeSettings rotationContainer, Identifier modelId) {
        return this;
    }

    @Override
    public boolean isVanillaAdapter() {
        return false;
    }
    private Sprite sprite;
    @Override
    public void emitBlockQuads(BlockRenderView blockView, BlockState state, BlockPos pos, Supplier<Random> randomSupplier, RenderContext context) {
        QuadEmitter emitter = context.getEmitter();
        if (sprite == null) sprite = RenderHelper.getModel(Blocks.GLASS.getDefaultState()).getSprite();
        Vector3f VICTIM = new Vector3f();
        context.pushTransform((quad) -> {
            for (int i = 0; i < 4; i++) {
                quad.copyPos(i, VICTIM);
                VICTIM.add(0.5F, 0, 0.5F);
                quad.pos(i, VICTIM);
            }
            return true;
        });

        for (int i = 0; i < 360; i += 18) {
            double current = Math.toRadians(i);
            double next = Math.toRadians(i+18);

            int light = RenderHelper.packLight(0xF, 0xF);
            float minU = sprite.getMinU();
            float maxU = sprite.getMaxU();
            float udelta = maxU-minU;
            minU += (udelta * 0.064F);
            maxU -= (udelta * 0.064F);

            emitter.pos(3, (float) Math.sin(current), 0F, (float) Math.cos(current));
            emitter.sprite(3, 0, minU, sprite.getMinV());
            emitter.pos(2, (float) Math.sin(next), 0F, (float) Math.cos(next));
            emitter.sprite(2, 0, maxU, sprite.getMinV());
            emitter.pos(1, (float) Math.sin(next), 1F, (float) Math.cos(next));
            emitter.sprite(1, 0, maxU, sprite.getMaxV());
            emitter.pos(0, (float) Math.sin(current), 1F, (float) Math.cos(current));
            emitter.sprite(0, 0, (float) minU, sprite.getMaxV());
            emitter.spriteColor(0, 0xFFFF_FFFF, 0xFFFF_FFFF, 0xFFFF_FFFF, 0xFFFF_FFFF);
            emitter.lightmap(light, light, light, light);
            emitter.emit();

            emitter.pos(0, (float) Math.sin(current), 0F, (float) Math.cos(current));
            emitter.sprite(0, 0, minU, sprite.getMinV());
            emitter.pos(1, (float) Math.sin(next), 0F, (float) Math.cos(next));
            emitter.sprite(1, 0, maxU, sprite.getMinV());
            emitter.pos(2, (float) Math.sin(next), 1F, (float) Math.cos(next));
            emitter.sprite(2, 0, maxU, sprite.getMaxV());
            emitter.pos(3, (float) Math.sin(current), 1F, (float) Math.cos(current));
            emitter.sprite(3, 0, (float) minU, sprite.getMaxV());
            emitter.spriteColor(0, 0xFFFF_FFFF, 0xFFFF_FFFF, 0xFFFF_FFFF, 0xFFFF_FFFF);
            emitter.lightmap(light, light, light, light);
            emitter.emit();
        }
        context.popTransform();
    }

    @Override
    public void emitItemQuads(ItemStack stack, Supplier<Random> randomSupplier, RenderContext context) {
        emitBlockQuads(null, null, null, randomSupplier, context);
    }

    @Override
    public List<BakedQuad> getQuads(BlockState state, Direction face, Random random) {
        return Collections.emptyList();
    }

    @Override
    public boolean useAmbientOcclusion() {
        return false;
    }

    @Override
    public boolean hasDepth() {
        return true;
    }

    @Override
    public boolean isSideLit() {
        return false;
    }

    @Override
    public boolean isBuiltin() {
        return false;
    }

    @Override
    public Sprite getSprite() {
        if (sprite == null) {
            sprite = RenderHelper.getModel(Blocks.GLASS.getDefaultState()).getSprite();
        }
        return sprite;
    }

    Transformation trans = new Transformation(new Vector3f(), new Vector3f(0, 0.25f, 0), new Vector3f(0.25f, 0.25f, 0.25f));
    ModelTransformation transform = new ModelTransformation(trans, trans, trans, trans, Transformation.IDENTITY, trans, trans, Transformation.IDENTITY);
    @Override
    public ModelTransformation getTransformation() {
        return transform;
    }

    @Override
    public ModelOverrideList getOverrides() {
        return ModelOverrideList.EMPTY;
    }
}
