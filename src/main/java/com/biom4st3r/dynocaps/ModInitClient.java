package com.biom4st3r.dynocaps;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.model.json.JsonUnbakedModel;
import net.minecraft.client.util.ModelIdentifier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.resource.Resource;
import net.minecraft.util.Identifier;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.model.ModelLoadingRegistry;
import net.fabricmc.fabric.api.client.rendereregistry.v1.BlockEntityRendererRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.WorldRenderEvents;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry;
import net.fabricmc.fabric.api.renderer.v1.Renderer;
import net.fabricmc.fabric.api.renderer.v1.RendererAccess;
import net.fabricmc.fabric.impl.client.indigo.renderer.IndigoRenderer;
import net.fabricmc.loader.api.FabricLoader;

import com.biom4st3r.dynocaps.blocks.DynocapBlockEntityRenderer;
import com.biom4st3r.dynocaps.blocks.DynocapDisplay.DynocapBlockEntity;
import com.biom4st3r.dynocaps.components.IDynoInventoryDIY;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.features.BoxRenderer;
import com.biom4st3r.dynocaps.guis.DynocapConfigGui;
import com.biom4st3r.dynocaps.guis.DynocapFilledGui;
import com.biom4st3r.dynocaps.guis.DynocapFilledScreen;
import com.biom4st3r.dynocaps.guis.NoPauseCottonScreen;
import com.biom4st3r.dynocaps.guis.capcase.CapCaseController;
import com.biom4st3r.dynocaps.guis.capcase.CapCaseGui;
import com.biom4st3r.dynocaps.items.DynocapItem;
import com.biom4st3r.dynocaps.register.BlockEntityEnum;
import com.biom4st3r.dynocaps.register.ItemEnum;
import com.biom4st3r.dynocaps.register.Packets;
import com.biom4st3r.dynocaps.register.ScreenHandlerEnum;
import com.biom4st3r.dynocaps.rendering.RoundModel;
import com.biom4st3r.dynocaps.util.DynocapModel;

public class ModInitClient implements ClientModInitializer {
    public static boolean doRendering = true;

    @Environment(EnvType.CLIENT)
    public static final Renderer getRenderer() {
        if (RendererAccess.INSTANCE.getRenderer() == null) return IndigoRenderer.INSTANCE;

        return RendererAccess.INSTANCE.getRenderer();
    }

    @Environment(EnvType.CLIENT)
    public static void openGUI(boolean filled) {
        if (!filled) {
            MinecraftClient.getInstance().openScreen(new NoPauseCottonScreen(new DynocapConfigGui()));
        } else {
            MinecraftClient.getInstance().openScreen(new DynocapFilledScreen(new DynocapFilledGui()));
        }
    }

    @Override
    public void onInitializeClient() {
        WorldRenderEvents.AFTER_TRANSLUCENT.register((context) -> {
            context.profiler().swap("dynocaps");
            BoxRenderer.render(context.matrixStack(), null, 1, context.camera().getPos());
        });
        Identifier ident = new Identifier("dynocap:block/dynocapblock");
        ModelIdentifier mid = new ModelIdentifier("dynocaps:dynoblock#inventory");
        ModelLoadingRegistry.INSTANCE.registerResourceProvider((manager) -> (id, context) -> id.equals(ident) ? new RoundModel() : null);
        ModelLoadingRegistry.INSTANCE.registerVariantProvider((manager) -> (modelId, context) -> modelId.equals(mid) ? new RoundModel() : null);
        ModelLoadingRegistry.INSTANCE.registerVariantProvider((manager) -> (modelId, context) -> {
            if (modelId.equals(new ModelIdentifier(new Identifier("dynocaps:basiccap"), "inventory")) && !FabricLoader.getInstance().isModLoaded("sodium") && ((DynocapItem) ItemEnum.Dynocap.asItem()).template.is3d()) {
                Resource model = null;

                try {
                    model = manager.getResource(new Identifier("dynocaps:models/item/dynocap_model.json"));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return new DynocapModel(JsonUnbakedModel.deserialize(new InputStreamReader(model.getInputStream())), 0xFFDDDDDD);
            }

            return null;
        });
        Packets.CLIENT.init();

        ColorProviderRegistry.ITEM.register((stack, layer) -> {
            if (layer == 0) {
                return 0xFFFFFFFF;
            }

            Optional<IDynocapComponent> Ocomponent;

            if ((Ocomponent = IDynocapComponent.TYPE.maybeGet(stack)).isPresent()) {
                IDynocapComponent component = Ocomponent.get();
                return component.getColor();
            } else {
                ModInit.logger.error("ColorProviderError0");
            }

            return 0xFF00FF;
        }, ItemEnum.DYNOCAPS.toArray(new Item[0]));

        ColorProviderRegistry.ITEM.register((stack, layer) -> {
            if (layer == 0) {
                return 0xFFFFFFFF;
            }

            IDynoInventoryDIY inv = IDynoInventoryDIY.TYPE.get(stack);

            if (inv.getStack(layer - 1).isEmpty()) {
                return 0xFFFFFFFF;
            }

            ItemStack cap = inv.getStack(layer - 1);
            return IDynocapComponent.TYPE.get(cap).getColor();
        }, ItemEnum.CapCase);

        ScreenRegistry.<CapCaseGui, CapCaseController>register(ScreenHandlerEnum.CAP_CASE.getType(), (handler, inventory, title) -> {
            return new CapCaseController(handler, inventory.player);
        });
        // ScreenRegistry.register(SifterGui.TYPE, (handler,inventory,title)-> {
        //     return new SifterController(handler, inventory.player);
        // });
        // ScreenRegistry.register(ScreenHandlerEnum.SIFTER.getType(), ScreenHandlerEnum.SIFTER.getClientFactory());


        BlockEntityRendererRegistry.INSTANCE.<DynocapBlockEntity>register(BlockEntityEnum.DISPLAY.asType(), dispatcher -> new DynocapBlockEntityRenderer(dispatcher));
    }
}
