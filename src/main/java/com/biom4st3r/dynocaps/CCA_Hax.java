package com.biom4st3r.dynocaps;

import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import com.biom4st3r.dynocaps.util.reflection.FieldRef.FieldHandler;

import dev.onyxstudios.cca.api.v3.component.ComponentContainer;
import dev.onyxstudios.cca.api.v3.component.ComponentKey;
import dev.onyxstudios.cca.internal.item.ItemComponentContainerFactory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

public class CCA_Hax {
    @SuppressWarnings({"deprecation"})
    public static void init() {
        ComponentContainer cc = new nerdhub.cardinal.components.api.util.container.AbstractComponentContainer<nerdhub.cardinal.components.api.component.Component>() {
            @Override
            public Set<ComponentKey<?>> keys() {
                return Collections.emptySet();
            }

            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return true;
            }

            @Override
            public nerdhub.cardinal.components.api.component.Component remove(Object key) {
                return null;
            }

            @Override
            public boolean remove(Object key, Object value) {
                return false;
            }

            @Override
            public void putAll(Map<? extends nerdhub.cardinal.components.api.ComponentType<?>, ? extends nerdhub.cardinal.components.api.component.Component> m) {
            }

            @Override
            public nerdhub.cardinal.components.api.component.Component putIfAbsent(nerdhub.cardinal.components.api.ComponentType<?> key, nerdhub.cardinal.components.api.component.Component value) {
                return null;
            }

            @Override
            public void clear() {
            }

            @Override
            public Collection<nerdhub.cardinal.components.api.component.Component> values() {
                return Collections.emptyList();
            }

            @Override
            public boolean containsValue(Object value) {
                return false;
            }

            @Override
            public Set<nerdhub.cardinal.components.api.ComponentType<?>> keySet() {
                return Collections.emptySet();
            }

            @Override
            public boolean containsKey(nerdhub.cardinal.components.api.ComponentType<?> key) {
                return false;
            }

            @Override
            public <T extends nerdhub.cardinal.components.api.component.Component> T get(nerdhub.cardinal.components.api.ComponentType<T> key) {
                return null;
            }

            @Override
            public nerdhub.cardinal.components.api.component.Component put(nerdhub.cardinal.components.api.ComponentType<?> key, nerdhub.cardinal.components.api.component.Component value) {
                return null;
            }

            @Override

            public Set<Entry<nerdhub.cardinal.components.api.ComponentType<?>, nerdhub.cardinal.components.api.component.Component>> entrySet() {
                return Collections.emptySet();
            }

            @Override
            public boolean hasComponents() {
                return true;
            }
        };

        try {
            Stream
                    .of(ItemStack.class.getDeclaredFields())
                    .filter(field -> field.getType() == ComponentContainer.class && (field.getModifiers() & Modifier.FINAL) == 0)
                    // .peek(field -> field.setAccessible(true))
                    .map(field -> FieldHandler.<ComponentContainer>get(ItemStack.class, field.getName(), -1))
                    .findFirst()
                    .ifPresent(
                        fh -> fh.set(ItemStack.EMPTY, cc)//,
                        // () -> {
                        //     throw new RuntimeException();}
                            ); // MixinItemStack$components

            Stream
					.of(Item.class.getDeclaredFields())
					.filter(field -> field.getType() == ItemComponentContainerFactory.class)
					// .peek(field -> field.setAccessible(true))
					.map(field -> FieldHandler.<ItemComponentContainerFactory>get(Item.class, field.getName(), -1))
					.findFirst()
					.ifPresent(
						fh -> fh.set(Items.AIR, (i, s) -> cc)//,
						// () -> {
						// 	throw new RuntimeException();}
                            ); // MixinItem$cardinal_containerFactory
        } catch (Throwable t) {
            for (String x : new String[]{"", "", "Dynocaps CCA HACKS FAILED!. Chunk corruption and Chunk bans may occur without!", "", ""}) {
                ModInit.logger.error(x);
            }
            System.out.println(t);
        }
    }
}
