package com.biom4st3r.dynocaps.util;

import java.util.function.Supplier;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.render.BufferBuilderStorage;
import net.minecraft.client.render.block.BlockModelRenderer;
import net.minecraft.client.render.block.BlockRenderManager;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.item.ItemRenderer;
import net.minecraft.client.render.model.BakedModelManager;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.Identifier;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

@Environment(EnvType.CLIENT)
public final class ClientHelper {
    public interface FloatSupplier {
        float get();
    }

    public static final MinecraftClient client = MinecraftClient.getInstance();
    public static final BufferBuilderStorage bufferBuilders = client.getBufferBuilders();
    public static final BakedModelManager bakedModelManager = client.getBakedModelManager();
    public static final ItemRenderer itemRenderer = client.getItemRenderer();
    public static final BlockRenderManager blockRenderManager = client.getBlockRenderManager();
    public static final EntityRenderDispatcher entityRenderDispatcher = client.getEntityRenderDispatcher();
    public static final Identifier BLOCK_ATLAS = SpriteAtlasTexture.BLOCK_ATLAS_TEXTURE;
    public static final Identifier PARTICLE_ATLAS = SpriteAtlasTexture.PARTICLE_ATLAS_TEXTURE;
    public static final Supplier<ClientPlayerEntity> player = (() -> client.player);
    public static final BlockModelRenderer blockModelRenderer = blockRenderManager.getModelRenderer();
    public static final Supplier<ClientWorld> world = (() -> player.get().clientWorld);
    public static final SpriteAtlasTexture BLOCK_ATLAS_TEXTURE = bakedModelManager.method_24153(BLOCK_ATLAS);
    public static final FloatSupplier tickDelta = () -> client.getTickDelta();
    public static class ATLASES {
        public static final Identifier
                BLOCKS = new Identifier("minecraft", "textures/atlas/blocks.png"),
                SIGNS = new Identifier("minecraft", "textures/atlas/signs.png"),
                SHIELD = new Identifier("minecraft", "textures/atlas/shield_patterns.png"),
                BANNER = new Identifier("minecraft", "textures/atlas/banner_patterns.png"),
                CHEST = new Identifier("minecraft", "textures/atlas/chest.png"),
                SHULKERS = new Identifier("minecraft", "textures/atlas/shulker_boxes.png"),
                BEDS = new Identifier("minecraft", "textures/atlas/beds.png");
    }

    public static SpriteAtlasTexture getTextureAtlas(Identifier id) {
        return bakedModelManager.method_24153(id);
    }
}
