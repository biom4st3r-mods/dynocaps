package com.biom4st3r.dynocaps.api;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.world.BlockView;
import net.minecraft.world.EntityView;
import net.minecraft.world.ModifiableWorld;
import net.minecraft.world.World;

public class ProtoWorld implements ModifiableWorld, BlockView, EntityView {
    private BlockContainer container;

    private Long2ObjectOpenHashMap<BlockEntity> bakedBlockEntities;
    private Long2IntOpenHashMap blockPosToIndex;
    private World world;
    private int width, height, depth;
    private List<ServerPlayerEntity> players = Lists.newArrayList();

    @Override
    public List<Entity> getOtherEntities(Entity except, Box box, Predicate<? super Entity> predicate) {
        return container
            .getEntities()
            .getEntities()
            .stream()
            .filter(predicate.and((e) -> e != except && box.contains(((Entity) e).getPos())))
            .collect(Collectors.toList());
    }

    @Override
    @SuppressWarnings({"unsafe", "unchecked"})
    public <T extends Entity> List<T> getEntitiesByClass(Class<? extends T> entityClass, Box box,
            Predicate<? super T> predicate) {
        return (List<T>) container
            .getEntities()
            .getEntities()
            .stream()
            .filter(((Predicate<? super Entity>) predicate).and((e) -> e.getClass().isInstance(entityClass)))
            .collect(Collectors.toList());
    }

    @Override
    public List<? extends PlayerEntity> getPlayers() {
        return players;
    }

    private boolean oob(BlockPos pos) {
        return pos.getX() >= 0 && pos.getY() >= 0 && pos.getZ() >= 0 && pos.getX() < width && pos.getY() < height && pos.getZ() < depth;
    }

    @Override
    public BlockEntity getBlockEntity(BlockPos pos) {
        if (oob(pos)) return null;
        return this.bakedBlockEntities.getOrDefault(pos.asLong(), null);
    }

    @Override
    public BlockState getBlockState(BlockPos pos) {
        if (oob(pos)) return Blocks.BARRIER.getDefaultState();
        int index = this.blockPosToIndex.getOrDefault(pos.asLong(), -1);
        if (index == -1) {
            return Blocks.BEDROCK.getDefaultState();
        }
        return container.states[index];
    }

    @Override
    public FluidState getFluidState(BlockPos pos) {
        return this.getBlockState(pos).getFluidState();
    }

    @Override
    public boolean setBlockState(BlockPos pos, BlockState state, int flags, int maxUpdateDepth) {
        if (oob(pos)) return false;
        // if (!blockPosToIndex.containsKey(pos.asLong())) {
        //     return false;
        // }
        int index = this.blockPosToIndex.get(pos.asLong());
        BlockState originalState = this.container.states[index];
        if (state.getBlock() instanceof BlockEntityProvider) {
            BlockEntity be = ((BlockEntityProvider) state.getBlock()).createBlockEntity(this);
            this.setBlockEntity(pos, be);
        }
        state.onBlockAdded(this.world, pos, state, (flags & 64) != 0);
        this.container.states[index] = state;
        if ((flags & 128) == 0 && state != originalState && (state.getOpacity(this, pos) != originalState.getOpacity(this, pos) || originalState.getLuminance() != state.getLuminance() || originalState.hasSidedTransparency() || originalState.hasSidedTransparency())) {
            // lighting
        }
        return false;
    }

    @Override
    public boolean removeBlock(BlockPos pos, boolean move) {
        if (oob(pos)) return false;
        return false;
    }

    @Override
    public boolean breakBlock(BlockPos pos, boolean drop, Entity breakingEntity, int maxUpdateDepth) {
        if (oob(pos)) return false;
        return false;
    }

    public void setBlockEntity(BlockPos pos, BlockEntity blockEntity) {
        this.bakedBlockEntities.put(pos.asLong(), blockEntity);
    }

    public void addEntity(Entity entity) {
        this.container.getEntities().addToWorld(entity);
    }
}
