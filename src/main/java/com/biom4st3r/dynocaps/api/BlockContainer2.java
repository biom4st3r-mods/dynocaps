package com.biom4st3r.dynocaps.api;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.NbtOps;
import net.minecraft.nbt.Tag;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockPos.Mutable;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.BlockRenderView;
import net.minecraft.world.LightType;
import net.minecraft.world.chunk.light.LightingProvider;
import net.minecraft.world.level.ColorResolver;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import blue.endless.jankson.annotation.Nullable;
import com.biom4st3r.dynocaps.api.BlockContainer.DynocapSerializationException;
import com.biom4st3r.dynocaps.mixin.rendering.BlockEntityAccessor;
import com.biom4st3r.dynocaps.util.ClientHelper;
import com.biom4st3r.dynocaps.util.EmptyCompoundTag;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import org.apache.commons.lang3.NotImplementedException;

public class BlockContainer2 extends MultiList<BlockInstance> {

    private static final String LOOKUP_COMPOUND_STATE = "lbs";

    /**
     * 
     */
    private static final String LOOKUP_INT_VAL = "f";

    /**
     * 
     */
    private static final String LOOKUP_LIST = "a";

    /**
     * IntArray based on Lookup map.
     */
    private static final String STATES_KEY = "b";

    /**
     * ListTag of CompoundTags.
     */
    private static final String ENTITY_TAGS = "c";

    /**
     * Index when saved.
     */
    private static final String INDEX_KEY = "d";

    /**
     * LongArray.
     */
    private static final String POSITIONS = "e";

    public BlockContainer2() {
        super(16, new Class<?>[]{BlockState.class,long.class,CompoundTag.class});
    }

    public BlockContainer2(int expectedSize) {
        this();
        this.reinit(expectedSize);
    }
    
    public static Tag serializeBlockState(BlockState state) {
        Tag tag = BlockState.CODEC.stable().encodeStart(NbtOps.INSTANCE, state).getOrThrow(true, (str) -> new DynocapSerializationException(str));
        return tag;
    }

    public static BlockState deserializeBlockState(Tag tag) {
        return BlockState.CODEC.stable().decode(NbtOps.INSTANCE, tag).getOrThrow(true, (str) -> new DynocapSerializationException(str)).getFirst();
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
        if (this.isEmpty()) {
            tag.putByte("empty", (byte) 1);
            return tag;
        }
        this.trim();
        tag.putLongArray(POSITIONS, this.getCollection(long[].class, 1));
        tag.putInt(INDEX_KEY, this.trueSize);
        ListTag etags = new ListTag();
        for (CompoundTag etag : this.getCollection(CompoundTag[].class, 2)) {
            etags.add(etag);
        }
        tag.put(ENTITY_TAGS, etags);
        // Building lookup map
        Object2IntOpenHashMap<BlockState> lookupMap = new Object2IntOpenHashMap<>();
        int lookupi = 0;
        for (BlockState state : this.getCollection(BlockState[].class, 0)) {
            int i = lookupMap.getOrDefault(state, -1);
            if (i == -1) {
                lookupMap.put(state, lookupi);
                lookupi++;
            }
        }
        BlockState[] states = this.getCollection(0);
        IntList blockstates = new IntArrayList(states.length);
        for (BlockState state : states) {
            blockstates.add(lookupMap.getInt(state));
        }
        tag.putIntArray(STATES_KEY, blockstates.toIntArray());

        ListTag lookupList = new ListTag();
        for (Object2IntMap.Entry<BlockState> entry : lookupMap.object2IntEntrySet()) {
            CompoundTag ct = new CompoundTag();
            ct.putInt(LOOKUP_INT_VAL, entry.getIntValue());
            ct.put(LOOKUP_COMPOUND_STATE, serializeBlockState(entry.getKey()));
            lookupList.add(ct);
        }
        tag.put(LOOKUP_LIST, lookupList);

        // this.getEntities().toTag(tag);

        return tag;
    }

    @Override
    public void fromTag(CompoundTag tag) {
        this.clear();
        if (tag.getByte("empty") == 1) return;
        this.reinit(tag.getInt(INDEX_KEY));
        this.trueSize = tag.getInt(INDEX_KEY);

        this.internal_copyInto(tag.getLongArray(POSITIONS), 1);

        ListTag entityTags = (ListTag) tag.get(ENTITY_TAGS);
        CompoundTag[] tags = new CompoundTag[entityTags.size()];
        for (int i = 0; i < entityTags.size(); i++) {
            tags[i] = entityTags.getCompound(i);
        }
        this.internal_copyInto(tags, 2);

        Int2ObjectMap<BlockState> lookupmap = new Int2ObjectOpenHashMap<>(10);
        ListTag lookupList = (ListTag) tag.get(LOOKUP_LIST);
        lookupList.forEach((t) -> {
            CompoundTag ct = (CompoundTag) t;
            lookupmap.put(ct.getInt(LOOKUP_INT_VAL), deserializeBlockState(ct.get(LOOKUP_COMPOUND_STATE)));
        });
        int[] statearray = tag.getIntArray(STATES_KEY);
        BlockState[] states = new BlockState[statearray.length];
        for (int i = 0; i < statearray.length; i++) {
            states[i] = lookupmap.get(statearray[i]);
        }
        this.internal_copyInto(states, 0);

        // this.getEntities().fromTag(tag);
    }

    @Override
    protected void set(int index, BlockInstance t) {
        this.<BlockState[]>getCollection(0)[index] = t.state;
        this.<long[]>getCollection(1)[index] = t.relativePos.asLong();
        this.<CompoundTag[]>getCollection(0)[index] = t.entityTag;
    }
    
    @Override
    public void clear() {
        this.view = null;
        super.clear();
    }

    @Override
    protected BlockInstance newT() {
        return new BlockInstance();
    }

    @Override
    protected void reassignT(int i, BlockInstance t) {
        t.reassign(this.<BlockState[]>getCollection(0)[i], this.<long[]>getCollection(1)[i], this.<CompoundTag[]>getCollection(2)[i]);
    }

    public final void add(BlockRenderView view, Vec3i referncePos, BlockPos pos) {
        BlockState state = view.getBlockState(pos);
        this.add(state, referncePos, pos, state.getBlock().hasBlockEntity() ? view.getBlockEntity(pos) : null);
    }

    public final void add(BlockState state, Vec3i referncePos, BlockPos pos, BlockEntity object) {
        this._rawAdd(state, pos.subtract(referncePos), object);
    }

    public void _rawAdd(BlockState state, BlockPos interalPos, @Nullable BlockEntity object) {
        this._rawAdd(state, interalPos, object == null ? EmptyCompoundTag.EMPTY : object.toTag(new CompoundTag()));
    }

    public void _rawAdd(BlockState state, BlockPos interalPos, CompoundTag object) {
        this.internal_add(()-> {
            this.<BlockState[]>getCollection(0)[trueSize] = state;
            this.<long[]>getCollection(1)[trueSize] = interalPos.asLong();
            this.<CompoundTag[]>getCollection(2)[trueSize] = object;
        });
    }

	public void addEmpty(BlockPos bottomLeft, Mutable pos) {
        this.add(Blocks.AIR.getDefaultState(), bottomLeft, pos, null);
	}

    /**
     * Creates BlockEntity from information at index i.
     *
     * @param i
     * @return
     */
    @Nullable
    @Environment(EnvType.CLIENT)
    public BlockEntity constructBlockEntity(int i) {
        
        if (i == -1 || this.getCollection(CompoundTag[].class, 2)[i].isEmpty()) {
            return null;
        } else {
            BlockEntity be;
            try {
                be = BlockEntity.createFromTag(this.getCollection(BlockState[].class, 0)[i], this.getCollection(CompoundTag[].class, 2)[i]);
            } catch(Throwable t) {
                t.printStackTrace();
                return null;
            }
            ((BlockEntityAccessor) be).setCachedState(this.getCollection(BlockState[].class, 0)[i]);
            ((BlockEntityAccessor) be).setWorld(ClientHelper.world.get());
            return be;
        }
    }

    @Environment(EnvType.CLIENT)
    ContainerBlockRenderView view;

    @Environment(EnvType.CLIENT)
    public BlockRenderView asBlockRenderView() {
        return view == null ? view = new ContainerBlockRenderView(this.trueSize) : view;
    }

    private class ContainerBlockRenderView implements BlockRenderView {
        Long2IntOpenHashMap blockPosToInstanceMap;
        ContainerBlockRenderView(int size) {
            blockPosToInstanceMap = new Long2IntOpenHashMap(size);
            for (int i = 0; i < trueSize; i++) {
                blockPosToInstanceMap.put(getCollection(long[].class, 1)[i], i);
            }
        }

        @Override
        public int getBaseLightLevel(BlockPos pos, int ambientDarkness) {
            return 12;
        }

        @Override
        public int getLightLevel(LightType type, BlockPos pos) {
            return 12;
        }

        @Override
        public int getMaxLightLevel() {
            return 12;
        }

        @Override
        public BlockEntity getBlockEntity(BlockPos pos) {
            return constructBlockEntity(blockPosToInstanceMap.getOrDefault(pos.asLong(), -1));
        }

        @Override
        public BlockState getBlockState(BlockPos pos) {
            int i = blockPosToInstanceMap.getOrDefault(pos.asLong(), -1);
            return i == -1 ? Blocks.AIR.getDefaultState() : getCollection(BlockState[].class, 0)[i];
        }

        @Override
        public FluidState getFluidState(BlockPos pos) {
            return getBlockState(pos).getFluidState();
        }

        @Override
        public float getBrightness(Direction direction, boolean shaded) {
            return 1.0F;
        }

        @Override
        public LightingProvider getLightingProvider() {
            throw new NotImplementedException("lighting provider is not used");
        }

        @Override
        public int getColor(BlockPos pos, ColorResolver colorResolver) {
            ClientPlayerEntity player = ClientHelper.player.get();
            return colorResolver.getColor(player.clientWorld.getBiome(player.getBlockPos()), player.getX(), player.getZ());
        }
    }
    @Override
    public int hashCode() {
        return System.identityHashCode(this.getCollection(0));
    }
}
