package com.biom4st3r.dynocaps.api;

import java.util.List;
import java.util.Map;

import net.minecraft.block.BlockState;
import net.minecraft.block.InventoryProvider;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.util.Clearable;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

import com.biom4st3r.dynocaps.util.reflection.MethodRef;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import net.fabricmc.loader.api.FabricLoader;

/**
 * Precomputed handles for after a {@code BlockEntity} has been captured.
 */
public interface OnBlockCapture {
    Map<BlockEntityType<?>, OnBlockCapture> actions = Util.make(() -> {
        Map<BlockEntityType<?>, OnBlockCapture> map = Maps.newHashMap();
        Class<?>[] TRADING_STATION_BLOCK_ENTITY = new Class<?>[1];
        for (BlockEntityType<?> type : Registry.BLOCK_ENTITY_TYPE) {
            BlockEntity blockEntity = type.instantiate();
            final List<OnBlockCapture> captures = Lists.newArrayList();
            if (blockEntity instanceof LootableContainerBlockEntity) {
                captures.add((world, pos, be, state, pe) -> {
                    ((LootableContainerBlockEntity) be).checkLootInteraction(null);
                    return true;
                });
            }
            if (blockEntity instanceof Clearable) {
                captures.add((world, pos, be, state, pe) -> {
                    ((Clearable) be).clear();
                    return true;
                });
            }
            if (blockEntity instanceof InventoryProvider) {
                captures.add((world, pos, be, state, pe) -> {
                    ((InventoryProvider) be).getInventory(state, world, pos).clear();
                    return true;
                });
            }
            if (FabricLoader.getInstance().isModLoaded("adorn")) {
                if (TRADING_STATION_BLOCK_ENTITY[0] == null) {
                    try {
                        TRADING_STATION_BLOCK_ENTITY[0] = Class.forName("juuxel.adorn.block.entity.TradingStationBlockEntity");
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                if (blockEntity.getClass() == TRADING_STATION_BLOCK_ENTITY[0]) {
                    MethodRef<Boolean> isOwner = MethodRef.getMethod(TRADING_STATION_BLOCK_ENTITY[0], "isOwner", PlayerEntity.class);
                    MethodRef<Inventory> getInventory = MethodRef.getMethod(TRADING_STATION_BLOCK_ENTITY[0], "getStorage");
                    captures.add((world, pos, be, state, pe) -> {
                        if (!isOwner.invoke(be, pe)) {
                            return false;
                        }
                        getInventory.invoke(be).clear();
                        return true;
                    });
                }
            }
            map.put(type, (world, pos, be, state, pe) -> {
                boolean bool = true;
                for (OnBlockCapture capture : captures) {
                    bool &= capture.postCapture(world, pos, be, state, pe);
                }
                return bool;
            });
        }

        return map;
    });

    /**
     * returning false will prevent capture of block and replace with air.
     * @param world
     * @param pos
     * @param be
     * @param state
     * @param pe
     * @return
     */
    boolean postCapture(World world, BlockPos pos, BlockEntity be, BlockState state, PlayerEntity pe);
}
