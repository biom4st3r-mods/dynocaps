package com.biom4st3r.dynocaps.guis;

import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.guis.widgets.HexTextField;
import com.biom4st3r.dynocaps.guis.widgets.WBioGridPanel;
import com.biom4st3r.dynocaps.register.Packets;
import com.biom4st3r.dynocaps.util.ClientHelper;

import io.github.cottonmc.cotton.gui.client.BackgroundPainter;
import io.github.cottonmc.cotton.gui.client.LightweightGuiDescription;
import io.github.cottonmc.cotton.gui.client.ScreenDrawing;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WLabel;
import io.github.cottonmc.cotton.gui.widget.WSlider;
import io.github.cottonmc.cotton.gui.widget.data.Axis;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.text.LiteralText;

public class DynocapConfigGui extends LightweightGuiDescription {

    @SuppressWarnings({"deprecation"})
    public static final BackgroundPainter bg = (x, y, panel) -> {
        ScreenDrawing.drawBeveledPanel(x-1, y-1, panel.getWidth()+2, panel.getHeight()+2);
    };

    IDynocapComponent component;

    private String getHex(int i) {
        String output = "";
        String string = Integer.toHexString(i);
        while (output.length() + string.length() < 6) {
            output += "0";
        }
        return String.format("%s%s", output, string).toUpperCase();
    }
    static class WSliderE extends WSlider {
        public boolean focusable = true;
        WSliderE(int min, int max, Axis axis) {
            super(min, max, axis);
        }

        @Override
        public boolean canFocus() {
            return focusable;
        }

        @Override
        public void onClick(int x, int y, int button) {
            if (focusable) super.onClick(x, y, button);
        }

        @Override
        protected boolean isMouseInsideBounds(int x, int y) {
            return focusable && super.isMouseInsideBounds(x, y);
        }
    }

    @SuppressWarnings({"deprecation"})
    public DynocapConfigGui() {
        ItemStack stack = ClientHelper.player.get().getMainHandStack();
        this.component = IDynocapComponent.TYPE.get(stack);
        boolean fixedSize = component.getTemplate().isFixedSize();
        boolean fixedColor = component.getTemplate().isFixedColor();

        WBioGridPanel root = new WBioGridPanel(22);
        this.setRootPanel(root);
        WLabel title = new WLabel("Dynocap");
        root.add(title, 0, 0);

        WLabel colorTitle = new WLabel("Color:");
        root.add(colorTitle, 0, 1);

        HexTextField colorField = new HexTextField(new LiteralText(getHex(component.getColor())));
        colorField.setEditable(true);
        colorField.setSize(6 * 18, 20);
        colorField.setMaxLength(6);
        String textString = Integer.toHexString(component.getColor());
        while (textString.length() < 6) textString = "0"+textString;
        colorField.setText(textString.toUpperCase());
        colorField.setEnabledColor(component.getColor());
        colorField.setDisabledColor(~component.getColor());
        colorField.setChangedListener((value) -> {
            if (value.isEmpty()) value = "000000";
            if (Integer.parseInt(value, 16) == component.getColor()) return;
            component.setColor(Integer.parseInt(value, 16));
            colorField.setEnabledColor(Integer.parseInt(value, 16));
            syncComp(component);
        });
        root.add(colorField, 2, 1, 2, 1);

        if (fixedColor) {
            WGridPanel disablePanel = new WGridPanel();
            disablePanel.setBackgroundPainter((x, y, widget) -> {
                MatrixStack matrix = new MatrixStack();
                matrix.translate(0, 0, 500);
                ScreenDrawing.coloredRect(x, y, root.getWidth(), 22, 0xCCc6_c6c6);
                ScreenDrawing.drawString(matrix, "Removed by", x, y, 0xFF993333);
                ScreenDrawing.drawString(matrix, "Manufacturer.", x, y+10, 0xFF993333);
            });
            colorField.focusable = false;
            disablePanel.setParent(root);
            root.add(disablePanel, 0, 1);
        }

        WSliderE widthSlider = new WSliderE(1, component.getTemplate().max_width, Axis.VERTICAL);
        widthSlider.setValue(component.getWidth());
        root.add(widthSlider, 1, 4);
        WLabel widthLabel = new WLabel(widthSlider.getValue()+"");
        root.add(widthLabel, 0, 4);
        WLabel widthTitle = new WLabel("Width");
        root.add(widthTitle, 0, 3);
        widthSlider.setValueChangeListener((i) -> {
            widthLabel.setText(new LiteralText(i+""));
            component.setWidth(i);
            syncComp(component);
        });

        WSliderE heightSlider = new WSliderE(1, component.getTemplate().max_height, Axis.VERTICAL);
        heightSlider.setValue(component.getHeight());
        root.add(heightSlider, 3, 4);
        WLabel heightLabel = new WLabel(heightSlider.getValue()+"");
        root.add(heightLabel, 2, 4);
        WLabel heightTitle = new WLabel("Height");
        root.add(heightTitle, 2, 3);
        heightSlider.setValueChangeListener((i) -> {
            heightLabel.setText(new LiteralText(i+""));
            component.setHeight(i);
            syncComp(component);
        });
        WSliderE depthSlider = new WSliderE(1, component.getTemplate().max_depth, Axis.VERTICAL);
        depthSlider.setValue(component.getDepth());
        root.add(depthSlider, 5, 4);
        WLabel depthLabel = new WLabel(depthSlider.getValue()+"");
        root.add(depthLabel, 4, 4);
        WLabel depthTitle = new WLabel("Depth");
        root.add(depthTitle, 4, 3);
        depthSlider.setValueChangeListener((i) -> {
            depthLabel.setText(new LiteralText(i+""));
            component.setDepth(i);
            syncComp(component);
        });

        if (fixedSize) {
            WGridPanel disablePanel = new WGridPanel();
            disablePanel.setBackgroundPainter((x, y, widget) -> {
                MatrixStack matrix = new MatrixStack();
                matrix.translate(0, 0, 500);
                // matrix.scale(2, 2, 2);
                ScreenDrawing.coloredRect(x, y, root.getWidth(), (22 * 2) + 1, 0xCCc6_c6c6);
                ScreenDrawing.drawString(matrix, "Removed by", x, y+10, 0xFF993333);
                ScreenDrawing.drawString(matrix, "Manufacturer.", x, (y+20), 0xFF993333);
            });
            depthSlider.focusable = false;
            heightSlider.focusable = false;
            widthSlider.focusable = false;
            disablePanel.setParent(root);
            root.add(disablePanel, 0, 3);
        }

        root.validate(this);
    }

    public static void syncComp(IDynocapComponent scomponent) {
        ModInit.logger.debug("syncComp");
        MinecraftClient.getInstance().getNetworkHandler().sendPacket(Packets.CLIENT.requestSettingsChange(scomponent));
    }
}
