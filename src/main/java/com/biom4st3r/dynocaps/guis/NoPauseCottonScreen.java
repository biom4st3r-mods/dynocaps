package com.biom4st3r.dynocaps.guis;

import io.github.cottonmc.cotton.gui.GuiDescription;
import io.github.cottonmc.cotton.gui.client.CottonClientScreen;
import net.minecraft.text.Text;

/**
 * NoPauseCottonScreen
 */
public class NoPauseCottonScreen extends CottonClientScreen {

    public NoPauseCottonScreen(Text title, GuiDescription description) {
        super(title, description);
    }
    
    public NoPauseCottonScreen(GuiDescription description) {
        super(description);
    }
    @Override
    public boolean isPauseScreen() {
        return false;
    }
    
}