package com.biom4st3r.dynocaps.guis;

import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;

import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.guis.widgets.HexTextField;
import com.biom4st3r.dynocaps.guis.widgets.UpdatingTextFeild;
import com.biom4st3r.dynocaps.guis.widgets.WBioGridPanel;
import com.biom4st3r.dynocaps.register.ItemEnum;
import com.biom4st3r.dynocaps.util.ClientHelper;

import io.github.cottonmc.cotton.gui.client.BackgroundPainter;
import io.github.cottonmc.cotton.gui.client.LightweightGuiDescription;
import io.github.cottonmc.cotton.gui.client.ScreenDrawing;
import io.github.cottonmc.cotton.gui.widget.WButton;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WLabel;
import io.github.cottonmc.cotton.gui.widget.WToggleButton;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtIo;
import net.minecraft.text.LiteralText;
import net.minecraft.text.TranslatableText;

public class DynocapFilledGui extends LightweightGuiDescription {

    @SuppressWarnings({"deprecation"})
    public static final BackgroundPainter bg = (x, y, panel) -> {
        ScreenDrawing.drawBeveledPanel(x - 1, y - 1, 400, 400);
    };
    IDynocapComponent component;

    @SuppressWarnings({"deprecation"})
    public DynocapFilledGui() {
        ItemStack stack = ClientHelper.player.get().getMainHandStack();
        this.component = IDynocapComponent.TYPE.get(stack);
        WBioGridPanel root = new WBioGridPanel(10);
        this.setRootPanel(root);
        this.getRootPanel().setSize(300, 150);
        WLabel title = new WLabel("DynoCap");
        WGridPanel renderPanel = new WGridPanel();
        renderPanel.setParent(root);
        renderPanel.setBackgroundPainter((x, y, panel) -> {
            ScreenDrawing.drawGuiPanel(x + 14, y - 5, 190, 160, 0xFF000000);
        });
        root.add(renderPanel, 10, 0);
        root.add(title, 0, 0);

        WLabel nameLabel = new WLabel("Name");
        root.add(nameLabel, 0, 2);
        UpdatingTextFeild nameField = new UpdatingTextFeild(new LiteralText(component.getName()));
        nameField.setSize(6 * 18, 20);
        nameField.setChangedListener((text) -> {
            this.component.setName(text);
            DynocapConfigGui.syncComp(component);
        });
        root.add(nameField, 0, 3, 11, 1);

        WLabel colorTitle = new WLabel("Color");
        root.add(colorTitle, 0, 6);

        HexTextField colorField = new HexTextField(new LiteralText(getHex(component.getColor())));
        colorField.setEditable(true);
        colorField.setSize(6 * 18, 20);
        colorField.setMaxLength(6);
        String textString = Integer.toHexString(component.getColor());
        while (textString.length() < 6)
            textString = "0" + textString;
        colorField.setText(textString.toUpperCase());
        colorField.setEnabledColor(component.getColor());
        colorField.setChangedListener((value) -> {
            int newcolor;
            if ((newcolor = Integer.parseInt(value, 16)) == component.getColor())
                return;
            this.component.setColor(newcolor);
            colorField.setEnabledColor(newcolor);
            DynocapConfigGui.syncComp(component);
        });
        root.add(colorField, 0, 7, 5, 1);

        if (component.getTemplate().isFixedColor()) {
            colorField.focusable = false;
            WGridPanel disablePanel = new WGridPanel();
            disablePanel.setBackgroundPainter((x, y, widget) -> {
                MatrixStack matrix = new MatrixStack();
                matrix.translate(0, 0, 500);
                ScreenDrawing.coloredRect(x-5, y-5, 50+40, 20+20, 0xCCc6_c6c6);
                ScreenDrawing.drawString(matrix, "Removed by", x, y+5, 0xFF993333);
                ScreenDrawing.drawString(matrix, "Manufacturer.", x, y+17, 0xFF993333);
            });
            disablePanel.setParent(root);
            disablePanel.setSize(5, 2);
            root.add(disablePanel, 0, 6, 5, 2);
        }

        WToggleButton placeAir = new WToggleButton(new TranslatableText("dialog.dynocaps.can_place_air"));
        placeAir.setToggle(component.getPlaceAir());
        placeAir.setOnToggle((state) -> {
            this.component.setPlaceAir(state);
            DynocapConfigGui.syncComp(component);
        });
        root.add(placeAir, 0, 10);

        WLabel sizeLabel = new WLabel(String.format("%sx%sx%s", this.component.getWidth(), this.component.getHeight(),
                this.component.getDepth()));
        root.add(sizeLabel, 0, 13);

        if (stack.getItem() == ItemEnum.AncientCap.asItem() && ClientHelper.player.get().isCreative()) {
            WButton serialize = new WButton(new LiteralText("Serialize"));
            serialize.setOnClick(() -> {
                Path path = FabricLoader.getInstance().getConfigDir().resolve("DynocapSerialization");
                File file = new File(path.toString(),
                        (component.getName().isEmpty()
                        ? "UnnamedDynocap" + ClientHelper.world.get().random.nextInt() : component.getName().toLowerCase()) + ".dynobin");
                file.setWritable(true);
                file.getParentFile().mkdirs();
                try {
                    FileOutputStream stream = new FileOutputStream(file);
                    DataOutput out = new DataOutputStream(stream);
                    CompoundTag tag = new CompoundTag();
                    component.writeToNbt(tag);
                    NbtIo.write(tag, out);
                    // stream.write(component.toTag(new CompoundTag()).getByteArray("v2"));
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            root.add(serialize, 6, 0, 5, 2);
        }

        root.validate(this);
    }

    @Override
    public void addPainters() {
        super.addPainters();
    }
    private String getHex(int i) {
        String output = "";
        String string = Integer.toHexString(i);
        while (output.length() + string.length() < 6) {
            output += "0";
        }
        return String.format("%s%s", output, string).toUpperCase();
    }
}
