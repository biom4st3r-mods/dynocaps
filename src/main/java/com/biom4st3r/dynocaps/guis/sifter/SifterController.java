package com.biom4st3r.dynocaps.guis.sifter;

import net.minecraft.entity.player.PlayerEntity;

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;

public class SifterController extends CottonInventoryScreen<SifterGui> {

    public SifterController(SifterGui description, PlayerEntity player) {
        super(description, player);
    }

    @Override
    public boolean isPauseScreen() {
        return false;
    }
}
