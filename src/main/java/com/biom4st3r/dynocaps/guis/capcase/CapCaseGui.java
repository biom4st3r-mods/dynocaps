package com.biom4st3r.dynocaps.guis.capcase;

import java.util.List;
import java.util.Set;

import com.biom4st3r.dynocaps.components.IDynoInventoryDIY;
import com.biom4st3r.dynocaps.register.ItemEnum;
import com.biom4st3r.dynocaps.register.ScreenHandlerEnum;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import io.github.cottonmc.cotton.gui.SyncedGuiDescription;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WItemSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.screen.slot.SlotActionType;

public class CapCaseGui extends SyncedGuiDescription {
    int forbiddenSlot = -1;
    List<WItemSlot> slotss = Lists.newArrayListWithCapacity(8);
    protected CapCaseGui(int syncId, PlayerInventory playerInventory) {
        super(ScreenHandlerEnum.CAP_CASE.getType(), syncId, playerInventory, getProperStack(playerInventory), null);

        WGridPanel root = new WGridPanel(9);
        
        this.setRootPanel(root);
        for (int i = 0; i < blockInventory.size(); i++) {
            slotss.add(WItemSlot.of(blockInventory, i));
        }

        root.add(slotss.get(0), 7, 0);
        root.add(slotss.get(1), 9, 0);
        root.add(slotss.get(2), 7, 2);
        root.add(slotss.get(3), 9, 2);

        root.add(this.createPlayerInventoryPanel(), 0, 5);
        root.validate(this);
    }

    public CapCaseGui(int syncId, PlayerInventory inventory, int slotid) {
        this(syncId, inventory);
        forbiddenSlot = slotid;
    }

    public static final Set<Item> acceptableBlocks = Sets.newHashSet(ItemEnum.Dynocap.asItem(), Items.AIR);
    @Override
    public ItemStack onSlotClick(int slotNumber, int button, SlotActionType action, PlayerEntity player) {
        if (slotNumber >= 0 && !acceptableBlocks.contains(getSlot(slotNumber).getStack().getItem())) return getSlot(slotNumber).getStack();
        return super.onSlotClick(slotNumber, button, action, player);
    }

    @Override
    public boolean canUse(PlayerEntity entity) {
        return entity.inventory.getStack(forbiddenSlot).getItem() == ItemEnum.CapCase.asItem();
    }

    public static Inventory getProperStack(PlayerInventory inv) {
        return IDynoInventoryDIY.TYPE.get(inv.player.getMainHandStack().getItem() == ItemEnum.CapCase.asItem() ? inv.player.getMainHandStack() : inv.player.getOffHandStack());
    }
}
