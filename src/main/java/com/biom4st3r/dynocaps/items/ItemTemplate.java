package com.biom4st3r.dynocaps.items;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

import com.biom4st3r.dynocaps.BioLogger;
import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.register.ItemEnum;
import com.biom4st3r.dynocaps.util.ResourceHandler;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import net.fabricmc.api.EnvType;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.registry.Registry;

public final class ItemTemplate {
    static final BioLogger logger = new BioLogger("DynocapJsonItems");
    static final Set<String> VALID_ATTRIBUTES = Sets.newHashSet("damageable", "breakable", "fixed_size", "fixed_color", "repairable", "3dmodel", "capturesEntities", "requiresEmptyArea", "usableInAdventureMode");
    Predicate<String> INVALID_ATTRIBUTE = (s) -> !VALID_ATTRIBUTES.contains(s);

    public static final ItemTemplate UNINIT_TEMPLATE = new ItemTemplate(
            "dynocap",
            10,
            10,
            10,
            5,
            5,
            5,
            "FF7545",
            -1,
            "Dynocap",
            Sets.newHashSet("3dmodel", "capturesEntities","requiresEmptyArea"),
            "minecraft:air",
            "DDDDDD",
            new Block[] {Blocks.BEDROCK, Blocks.COMMAND_BLOCK, Blocks.CHAIN_COMMAND_BLOCK, Blocks.REPEATING_COMMAND_BLOCK, Blocks.BARRIER, Blocks.STRUCTURE_BLOCK, Blocks.END_PORTAL_FRAME, Blocks.END_PORTAL_FRAME, Blocks.NETHER_PORTAL, Blocks.END_GATEWAY},
            new Block[] {Blocks.BEDROCK, Blocks.COMMAND_BLOCK, Blocks.CHAIN_COMMAND_BLOCK, Blocks.REPEATING_COMMAND_BLOCK, Blocks.BARRIER, Blocks.STRUCTURE_BLOCK, Blocks.END_PORTAL_FRAME, Blocks.END_PORTAL_FRAME, Blocks.NETHER_PORTAL, Blocks.END_GATEWAY},
            new EntityType<?>[0]);

    public static void replaceDefaultCap(int width, int depth, int height, HashSet<String> hashSet) {
        DEFAULT = new ItemTemplate(
                "dynocap",
                width,
                depth,
                height,
                width/2,
                depth/2,
                height/2,
                "FF7545",
                -1,
                "Dynocap",
                Sets.newHashSet(),
                "minecraft:air",
                "DDDDDD",
                new Block[0],
                new Block[0],
                new EntityType<?>[0]);
    }

    public static void resetDefaultCap() {
        DEFAULT = UNINIT_TEMPLATE;
    }

    public static ItemTemplate DEFAULT = UNINIT_TEMPLATE;
    public static ItemTemplate LOAD_DEFAULT;

    public static final ItemTemplate ANCIENT_CAP = new ItemTemplate(
            "rustycap",
            85,
            85,
            85,
            10,
            10,
            10,
            "FF7545",
            -1,
            "Rusty Cap",
            Sets.newHashSet(),
            "minecraft:air",
            "DDDDDD",
            new Block[0],
            new Block[0],
            new EntityType<?>[0]);

    public final String id;

    public final int max_width;
    public final int max_depth;
    public final int max_height;

    public final int default_width;
    public final int default_depth;
    public final int default_height;

    public final int color;

    public final int max_damage;

    public final String default_name;

    public final Set<String> attributes;

    public final Item repairItem;

    private final int modelBaseColor;

    public final Set<Block> preventCaptureBlocks;

    public final Set<Block> preventOverrideBlocks;

    public final Set<EntityType<?>> preventCaptureEntities;

    private ItemTemplate(
            String id, int max_width, int max_depth,
            int max_height, int default_width, int default_height,
            int default_depth, String color, int max_damage,
            String default_name, Set<String> attributes, String repairItem,
            String modelBaseColor, Block[] preventCaptureBlocks,
            Block[] preventOverrideBlocks, EntityType<?>[] preventCaptureEntity) {
        this.id = id;
        this.max_width = max_width;
        ASSERT_MIN(max_width, 1, "max_width must be greater than 0");
        this.max_depth = max_depth;
        ASSERT_MIN(max_depth, 1, "max_depth must be greater than 0");
        this.max_height = max_height;
        ASSERT_MIN(max_height, 1, "max_height must be greater than 0");
        this.default_width = default_width > 0 ? default_width : max_width;
        this.default_height = default_height > 0 ? default_height : max_height;
        this.default_depth = default_depth > 0 ? default_depth : max_depth;
        this.color = Integer.parseInt(color, 16);
        this.max_damage = max_damage;
        this.default_name = default_name;
        this.attributes = attributes;
        attributes.stream().filter(INVALID_ATTRIBUTE).findFirst().ifPresent((s) -> {
            throw new AssertionError("%a is not a valid attribute in item %b".replace("%a", s).replace("%b", this.id));
        });
        this.repairItem = Registry.ITEM.get(new Identifier(repairItem));
        this.modelBaseColor = Integer.parseInt(modelBaseColor, 16);
        this.preventCaptureBlocks = Sets.newHashSet(preventCaptureBlocks);
        this.preventOverrideBlocks = Sets.newHashSet(preventOverrideBlocks);
        this.preventCaptureEntities = Sets.newHashSet(preventCaptureEntity);
    }

    public int getModelBaseColor() { // Patch for serialization returning FFDDDDDD
        return 0xFF000000 | this.modelBaseColor;
    }

    public boolean isDamageable() {
        return attributes.contains("damageable");
    }
    public boolean isFixedSize() {
        return attributes.contains("fixed_size");
    }
    public boolean isFixedColor() {
        return attributes.contains("fixed_color");
    }
    public boolean isBreakable() {
        return attributes.contains("breakable");
    }
    public boolean isRepairable() {
        return attributes.contains("repairable");
    }
    public boolean canCaptureEntities() {
        return attributes.contains("capturesEntities");
    }
    public boolean is3d() {
        return attributes.contains("3dmodel");
    }
    public boolean requiresEmptyArea() {
        return attributes.contains("requiresEmptyArea");
    }
    public boolean usableInAdventureMode() {
        return attributes.contains("usableInAdventureMode");
    }

    private static void ASSERT_MIN(int val, int min, String message) {
        if (val < min) throw new AssertionError(message);
    }
    private static void ASSERT(boolean val, String message) {
        if (!val) throw new AssertionError(message);
    }

    public Item register() {
        Identifier identifier = this.id.split(":").length == 2 ? new Identifier(this.id) : new Identifier(ModInit.MODID, this.id);
        if (Registry.ITEM.containsId(identifier)) {
            logger.log("Skipped re-registering %s", this.id);
            return Registry.ITEM.get(identifier);
        }
        if (FabricLoader.getInstance().getEnvironmentType() == EnvType.CLIENT) {
            if (FabricLoader.getInstance().isModLoaded("sodium")) logger.error("3dmodel dynocaps are not compatible with Sodium and thus the settings is being ignored");
            ItemTemplateClient.client_register(this, identifier);
        }

        Item.Settings settings = new Item.Settings().group(ModInit.DYNO_GROUP);
        if (attributes.contains("damageable")) {
            ASSERT_MIN(this.max_damage, 0, "Dynocap %s has attribute 'damageable', but did not set a 'max_damage' value".replace("%s", this.id));
            settings.maxDamage(this.max_damage);
        }
        DynocapItem item;
        if (this.isRepairable()) {
            ASSERT(this.isDamageable(), "Dynocap %s has attirbute 'repairable', but doesn't have attribute 'damageable'".replace("%s", this.id));
            ASSERT(this.repairItem != Items.AIR, "Dynocap %s has attribute 'repairable', but 'repair_item' is minecraft:air".replace("%s", this.id));
            item = new DynocapItem(settings, this) {
                @Override
                public boolean canRepair(ItemStack stack, ItemStack ingredient) {
                    return ingredient.getItem() == repairItem;
                }
            };
        } else item = new DynocapItem(settings, this);
        ResourceHandler.logger.debug(identifier.toString());
        Registry.register(Registry.ITEM, identifier, item);
        ItemEnum.DYNOCAPS.add(item);

        return item;
    }

    @SuppressWarnings({"unchecked"})
    public static <T> T[] jsonToJavaArray(JsonArray array, Class<?> type, Function<String, T> funct) {
        T[] out = (T[]) Array.newInstance(type, array.size());
        for (int i = 0; i < array.size(); i++) {
            out[i] = funct.apply(array.get(i).getAsString());
        }
        return out;
    }

    public static final class Deserial implements JsonDeserializer<ItemTemplate> {
        @Override
        public ItemTemplate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject object = json.getAsJsonObject();
            ImmutableSet.Builder<String> builder = new ImmutableSet.Builder<>();
            JsonHelper.getArray(object, "attributes").forEach(ele -> {
                builder.add(ele.getAsString());
            });
            return new ItemTemplate(
                JsonHelper.getString(object, "id"),
                JsonHelper.getInt(object, "max_width"),
                JsonHelper.getInt(object, "max_depth"),
                JsonHelper.getInt(object, "max_height"),
                JsonHelper.getInt(object, "default_width", 0),
                JsonHelper.getInt(object, "default_depth", 0),
                JsonHelper.getInt(object, "default_height", 0),
                JsonHelper.getString(object, "color", "FF00FF"),
                JsonHelper.getInt(object, "max_damage", -1),
                JsonHelper.getString(object, "default_name", "Dynocap"),
                builder.build(),
                JsonHelper.getString(object, "repair_item", "minecraft:air"),
                JsonHelper.getString(object, "model_color", "DDDDDD"),
                jsonToJavaArray(JsonHelper.getArray(object, "preventCaptureBlocks", new JsonArray()), Block.class, (string) -> Registry.BLOCK.get(new Identifier(string))),
                jsonToJavaArray(JsonHelper.getArray(object, "preventOverrideBlocks", new JsonArray()), Block.class, (string) -> Registry.BLOCK.get(new Identifier(string))),
                jsonToJavaArray(JsonHelper.getArray(object, "preventCaptureEntities", new JsonArray()), EntityType.class, (string) -> Registry.ENTITY_TYPE.get(new Identifier(string)))
                );
        }
    }

    public static final class Serializer implements JsonSerializer<ItemTemplate> {
        @Override
        public JsonElement serialize(ItemTemplate src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject object = new JsonObject();
            object.addProperty("id", src.id);
            object.addProperty("max_width", src.max_width);
            object.addProperty("max_depth", src.max_depth);
            object.addProperty("max_height", src.max_height);
            object.addProperty("default_width", src.default_width);
            object.addProperty("default_depth", src.default_depth);
            object.addProperty("default_height", src.default_height);
            object.addProperty("color", Integer.toHexString(src.color));
            object.addProperty("max_damage", src.max_damage);
            object.addProperty("default_name", src.default_name);
            JsonArray array = new JsonArray();
            for (String s : src.attributes) {
                array.add(s);
            }
            object.add("attributes", array);

            object.addProperty("repair_item", Registry.ITEM.getId(src.repairItem).toString());
            object.addProperty("model_color", Integer.toHexString(src.modelBaseColor));

            array = new JsonArray();
            for (Block block : src.preventCaptureBlocks) {
                array.add(Registry.BLOCK.getId(block).toString());
            }
            object.add("preventCaptureBlocks", array);

            array = new JsonArray();
            for (Block block : src.preventOverrideBlocks) {
                array.add(Registry.BLOCK.getId(block).toString());
            }
            object.add("preventOverrideBlocks", array);

            array = new JsonArray();
            for (EntityType<?> block : src.preventCaptureEntities) {
                array.add(Registry.ENTITY_TYPE.getId(block).toString());
            }
            object.add("preventCaptureEntities", array);

            return object;
        }
    }
}
