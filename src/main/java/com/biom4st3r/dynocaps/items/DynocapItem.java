package com.biom4st3r.dynocaps.items;

import java.util.List;

import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.components.IDynocapComponent;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

public class DynocapItem extends Item {
    public DynocapItem(Item.Settings settings, ItemTemplate template) {
        super(settings.group(ModInit.DYNO_GROUP));
        this.template = template;
    }

    public final ItemTemplate template;

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack stack = user.getStackInHand(hand);
        IDynocapComponent component = IDynocapComponent.TYPE.get(stack);

        TypedActionResult<ItemStack> result = DynocapHelper.baseUse(world, user, stack, component);
        if (result.getResult() != ActionResult.PASS)
            return result;

        if (!component.isFilled()) {
            return DynocapHelper.useEmpty((ServerWorld) world, user, hand, stack, component);
        } else {
            return DynocapHelper.useFilled((ServerWorld) world, user, stack, component);
        }
    }

    @Environment(EnvType.CLIENT)
    @Override
    public Text getName(ItemStack stack) {
        if (stack != null) {
            IDynocapComponent component = IDynocapComponent.TYPE.get(stack);
            if (!component.getName().isEmpty()) {
                return new LiteralText(component.getName());
            }
        }
        return super.getName(stack);
    }

    @Environment(EnvType.CLIENT)
    @Override
    public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
        if (IDynocapComponent.TYPE.get(stack).isFilled()) {
            tooltip.add(new TranslatableText("dialog.dynocaps.filled").formatted(Formatting.GRAY));
        }
    }
}
