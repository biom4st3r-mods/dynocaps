package com.biom4st3r.dynocaps.items;

import java.util.List;

import com.biom4st3r.dynocaps.components.IDynocapComponent;

import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.explosion.Explosion.DestructionType;

public class AncientCapItem extends DynocapItem {
    public AncientCapItem(Settings settings) {
        super(settings, ItemTemplate.ANCIENT_CAP);
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack sourceStack = user.getStackInHand(hand);
        IDynocapComponent component = IDynocapComponent.TYPE.get(sourceStack);
        TypedActionResult<ItemStack> result = DynocapHelper.baseUse(world, user, sourceStack, component);
        if (result.getResult() != ActionResult.PASS) return result;

        if (component.isFilled()) {
            if (user.isCreative()) {
                return DynocapHelper.useFilled((ServerWorld) world, user, sourceStack, component);
            } else {
                result = DynocapHelper.useFilled((ServerWorld) world, user, sourceStack, component);

                user.addStatusEffect(new StatusEffectInstance(StatusEffects.RESISTANCE, 5, 128, true, false)); // Easy hax
                BlockPos pos = user.getBlockPos().offset(user.getHorizontalFacing());
                world.createExplosion(null, pos.getX(), pos.getY(), pos.getZ(), 30.0F, false, DestructionType.NONE);
                return TypedActionResult.consume(ItemStack.EMPTY);
            }
        } else {
            if (user.isCreative()) {
                return DynocapHelper.useEmpty((ServerWorld) world, user, hand, sourceStack, component);
            }
        }
        return TypedActionResult.pass(sourceStack);
    }

    @Override
    public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
        super.appendTooltip(stack, world, tooltip, context);
        tooltip.add(new TranslatableText("dialog.dynocaps.damagedcapsWarning0"));
        tooltip.add(new TranslatableText("dialog.dynocaps.damagedcapsWarning1"));
    }
}
