package com.biom4st3r.dynocaps.items;

import com.biom4st3r.dynocaps.ModInitClient;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.components.IDynocapComponent.Result;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.GameMode;
import net.minecraft.world.World;

public interface DynocapHelper {
    /**
     * Return success/fail/consume if you should cancel further execution.
     *
     * <p>Also blocks isClient from continuing
     * @param world
     * @param user
     * @param sourceStack
     * @param component
     * @return
     */
    static TypedActionResult<ItemStack> baseUse(World world, PlayerEntity user, ItemStack sourceStack, IDynocapComponent component) {
        if (user.isSneaking()) {
            if (world.isClient) {
                component.getCache().clear();
                component.getRenderer().clear();
                ModInitClient.openGUI(component.isFilled());
            }
            return TypedActionResult.success(sourceStack);
        }

        Result r = adventureModeCheck(user, sourceStack);
        if (!r.isSuccess()) return r.finish(user, sourceStack);

        user.getItemCooldownManager().set(sourceStack.getItem(), 17);
        if (world.isClient) {
            component.getCache().clear();
            component.getRenderer().clear();
            return TypedActionResult.success(sourceStack);
        }
        if (component.getTemplate().isDamageable() && component.getTemplate().isBreakable() & sourceStack.getDamage() == sourceStack.getMaxDamage() - 1) {
            return TypedActionResult.fail(sourceStack);
        }
        return TypedActionResult.pass(sourceStack);
    }

    static Result adventureModeCheck(PlayerEntity user, ItemStack stack) {
        if (!((DynocapItem) stack.getItem()).template.usableInAdventureMode() && user instanceof ServerPlayerEntity && ((ServerPlayerEntity) user).interactionManager.getGameMode() == GameMode.ADVENTURE) {
            return Result.ADVENTURE_MODE;
        }
        return Result.SUCCESS;
    }

    static TypedActionResult<ItemStack> useFilled(ServerWorld world, PlayerEntity user, ItemStack sourceStack, IDynocapComponent component) {
        Direction dir = user.getHorizontalFacing();
        Result r = component.releaseBlocks(world, offsetBlockPos(user.getBlockPos(), dir, component), user);

        return r.finish(user, sourceStack);
    }

    static BlockPos offsetBlockPos(BlockPos playerPos, Direction dir, IDynocapComponent component) {
        if (dir == Direction.NORTH) return playerPos.add(-component.getWidth()/2, 0, -component.getDepth()-1);
        else if (dir == Direction.EAST) return playerPos.add(2, 0, -component.getDepth()/2);
        else if (dir == Direction.SOUTH) return playerPos.add(-component.getWidth()/2, 0, 2);
        else if (dir == Direction.WEST) return playerPos.add(-component.getWidth()-1, 0, -component.getDepth()/2);
        else throw new IllegalStateException("Direction should not be up or down!");
    }

    static TypedActionResult<ItemStack> useEmpty(ServerWorld world, PlayerEntity user, Hand hand, ItemStack sourceStack, IDynocapComponent component) {
        Direction dir = user.getHorizontalFacing();
        if (component.getTemplate().isDamageable()) {
            sourceStack.damage(1, user, (p) -> {
                p.sendToolBreakStatus(hand);
            });
        }
        Result result = component.captureBlocks(world, offsetBlockPos(user.getBlockPos(), dir, component), user, true);
        return result.finish(user, sourceStack);
    }
}
