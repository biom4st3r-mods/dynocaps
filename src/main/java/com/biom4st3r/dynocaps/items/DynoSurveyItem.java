package com.biom4st3r.dynocaps.items;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.chunk.ChunkSection;
import net.minecraft.world.chunk.PalettedContainer.CountConsumer;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap.Entry;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;

public class DynoSurveyItem extends Item {
    public static Map<UUID, CompletableFuture<SurveyResults>> tasks = Maps.newHashMap();

    public DynoSurveyItem(Settings settings) {
        super(settings);
    }

    static class SurveyResults {
        public Object2IntMap<Block> map = new Object2IntOpenHashMap<>();
        public int total;
        public List<Text> messages = Lists.newArrayList();

        public void finish() {
            for (Entry<Block> e : map.object2IntEntrySet()) {
                float percent = ((float) e.getIntValue()/(float) total) * 100;
                if (percent > 0.039) {
                    messages.add(new TranslatableText(e.getKey().getTranslationKey()).append(String.format(": %.4f", percent)));
                }
            }
            map.clear();
            map = null;
        }
    }

    @Override
    @SuppressWarnings("resource")
    public ActionResult useOnBlock(ItemUsageContext context) {
        PlayerEntity player = context.getPlayer();
        BlockPos pos = context.getBlockPos();
        if (!context.getWorld().isClient) {
            ServerWorld w = (ServerWorld) context.getWorld();
            if (tasks.containsKey(player.getUuid())) {
                CompletableFuture<SurveyResults> f = tasks.get(player.getUuid());
                if (f.isDone()) {
                    SurveyResults result = f.getNow(null);
                    if (result != null) {
                        player.sendMessage(new LiteralText("Survey Results:"), false);
                        for (Text text : result.messages) {
                            player.sendMessage(text, false);
                        }
                        tasks.remove(player.getUuid());
                    }
                } else {
                    player.sendMessage(new LiteralText("Analyzing soil..."), false);
                    return ActionResult.FAIL;
                }
            } else {
                tasks.put(player.getUuid(),
                        CompletableFuture.supplyAsync(() -> {
                            SurveyResults results = new SurveyResults();
                            ChunkSection[] slices = w.getChunk(pos).getSectionArray();
                            CountConsumer<BlockState> consumer = (state, count) -> {
                                if (!state.isToolRequired()) return;
                                results.total += count;
                                int i = results.map.getOrDefault(state.getBlock(), 0);
                                results.map.put(state.getBlock(), i + count);
                            };
                            for (ChunkSection section : slices) {
                                if (section == null) continue;
                                section.lock();
                                section.getContainer().count(consumer);
                                section.unlock();
                            }
                            results.finish();
                            return results;
                        }, w.getServer()));
            }
        }
        return ActionResult.SUCCESS;
    }
}
