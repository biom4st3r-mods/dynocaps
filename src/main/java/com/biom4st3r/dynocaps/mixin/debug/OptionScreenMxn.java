package com.biom4st3r.dynocaps.mixin.debug;

import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.options.OptionsScreen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;

import com.biom4st3r.dynocaps.ModInitClient;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(OptionsScreen.class)
public abstract class OptionScreenMxn extends Screen {
    protected OptionScreenMxn(Text title) {
        super(title);
    }

    @Inject(
            at = @At("TAIL"),
            method = "init",
            cancellable = true,
            locals = LocalCapture.NO_CAPTURE)
    public void CHANGEMEAAAA(CallbackInfo ci) {
        if (FabricLoader.getInstance().isDevelopmentEnvironment())
            this.addButton(new ButtonWidget(this.width/2 + 100, 80, 150, 20, new LiteralText("Toggle Dynocap Rendering"), (widget) -> {
                ModInitClient.doRendering = !ModInitClient.doRendering;
            }));
    }
}
