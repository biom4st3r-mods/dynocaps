package com.biom4st3r.dynocaps.register;

import java.util.List;

import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.items.AncientCapItem;
import com.biom4st3r.dynocaps.items.DynoCapCaseItem;
import com.biom4st3r.dynocaps.items.DynoSurveyItem;
import com.biom4st3r.dynocaps.items.DynocapItem;
import com.biom4st3r.dynocaps.items.ItemTemplate;
import com.google.common.collect.Lists;

import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.Item.Settings;
import net.minecraft.item.ItemConvertible;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public enum ItemEnum implements ItemConvertible {
    Dynocap("basiccap",
        new DynocapItem(new Settings().maxCount(1), ItemTemplate.DEFAULT)),
    AncientCap("ancientcap",
        new AncientCapItem(new Settings().maxCount(1))),
    Dynocap_Top("compressor",
        new Item(new Settings().group(ModInit.DYNO_GROUP).maxCount(8))),
    Dynocap_Bottom("decompressor",
        new Item(new Settings().group(ModInit.DYNO_GROUP).maxCount(8))),
    CapCase("cap_case",
        new DynoCapCaseItem()),
    Surveyer("surveyer",
        new DynoSurveyItem(new Settings().group(ModInit.DYNO_GROUP).maxCount(1))),
    Dynocap_Block("dynoblock",
        new BlockItem(BlockRegister.EBlock.DISPLAY.asBlock(), new Settings().group(ModInit.DYNO_GROUP))),;

    ItemEnum(String name, Item i) {
        this.item = Registry.register(Registry.ITEM, new Identifier(ModInit.MODID, name), i);
    }

    Item item;

    @Override
    public Item asItem() {
        return item;
    }
    public static void loadClass() {
    }

    public static List<Item> DYNOCAPS = Lists.newArrayList(Dynocap.asItem(), AncientCap.asItem());
}
