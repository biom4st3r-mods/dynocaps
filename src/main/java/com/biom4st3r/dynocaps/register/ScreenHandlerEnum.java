package com.biom4st3r.dynocaps.register;

import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.util.Identifier;

import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry.ExtendedClientHandlerFactory;

import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.guis.capcase.CapCaseGui;
import com.biom4st3r.dynocaps.guis.sifter.SifterGui;

public enum ScreenHandlerEnum {
    SIFTER("sifter", (syncid,inv,buff)->new SifterGui(syncid,inv)),//,(handler,inv,title)->new SifterController(handler,inv.player)),
    CAP_CASE("dynocap_gui", (syncid, playerInv, buff)->new CapCaseGui(syncid, playerInv, buff.readByte())),
    ;
    public Identifier id;
    private ScreenHandlerType<?> TYPE;
    private ExtendedClientHandlerFactory<?> handlerFactory;

    // private Factory<?,?> clientFactory;
    // @SuppressWarnings({"unchecked"})
    // public <H extends ScreenHandler, S extends Screen & ScreenHandlerProvider<H>> Factory<H,S> getClientFactory() {
    //     return (Factory<H, S>) clientFactory;
    // }

    @SuppressWarnings({"unchecked"})
    public  <H extends ScreenHandler> ScreenHandlerType<H> getType() {
        return (ScreenHandlerType<H>) this.TYPE;
    }

    <HANDLER extends ScreenHandler>ScreenHandlerEnum(String name, ExtendedClientHandlerFactory<HANDLER> factory) {//, ScreenRegistry.Factory<HANDLER,SCREEN> clientFactory) {
        this.id = new Identifier(ModInit.MODID,name);
        this.handlerFactory = factory;
        // this.clientFactory = clientFactory;
        this.TYPE = ScreenHandlerRegistry.registerExtended(id, handlerFactory);
    }
    public static void loadClass() {
    }
}
