package com.biom4st3r.dynocaps.register;

import java.util.List;
import java.util.stream.Collectors;

import com.biom4st3r.dynocaps.api.BlockContainer;
import com.biom4st3r.dynocaps.components.IDynocapComponent;

import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;

public class TestCommand {
    static int index = 0;

    public static void init() {
        CommandRegistrationCallback.EVENT.register((dispatcher, isServer) -> {
            dispatcher.register(
                    CommandManager
                        .literal("dynocaps:test_all_blocks")
                        .requires((source) -> source.hasPermissionLevel(2))
                        .executes((context) -> {
                            new Thread(() -> {
                                List<Block> blocks = Registry.BLOCK.stream().skip(index).collect(Collectors.toList());
                                Class<ClientPlayerEntity> clazz = ClientPlayerEntity.class;
                                clazz.getName();
                                // Check to make sure it is client only
                                // Also crashing this thread is safe

                                BlockContainer container = new BlockContainer(1);
                                for (int i = 0; i < blocks.size(); i++) {
                                    index = i;
                                    Block block = blocks.get(i);
                                    ServerPlayerEntity pe = ((ServerPlayerEntity) context.getSource().getEntity());

                                    container._rawAdd(block.getDefaultState(), new BlockPos(0, 0, 0), block.hasBlockEntity() ? ((BlockEntityProvider) block).createBlockEntity(pe.getEntityWorld()) : null);
                                    System.out.println(block.getLootTableId());
                                    System.out.println(i + "/" + blocks.size());
                                    ItemStack is = new ItemStack(ItemEnum.Dynocap);
                                    IDynocapComponent component = IDynocapComponent.TYPE.get(is);
                                    component.setDepth(1);
                                    component.setWidth(1);
                                    component.setHeight(1);
                                    component.getContainer().fromTag(container.toTag(new CompoundTag()));
                                    component.getCache().clear();
                                    component.getRenderer().clear();
                                    pe.setStackInHand(Hand.MAIN_HAND, is);
                                    pe.updateCursorStack();
                                    try {
                                        Thread.sleep(100);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    container.reinit(1);
                                }
                                index = 0;
                            }, "Bruh").start();
                            return 1;
                        })
            );
        });
    }
}
