package com.biom4st3r.dynocaps.register;

import java.util.function.Supplier;

import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.blocks.DynoSifterBE;
import com.biom4st3r.dynocaps.blocks.DynocapDisplay;

import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public enum BlockEntityEnum {
    DISPLAY("display", () -> new DynocapDisplay.DynocapBlockEntity(), BlockEnum.DISPLAY.asBlock()),
    SIFTER("sifter", () -> new DynoSifterBE(), BlockEnum.SIFTER.asBlock()),;

    BlockEntityEnum(String id, Supplier<BlockEntity> supplier, Block... blocks) {
        this.type = BlockEntityType.Builder.create(supplier, blocks).build(null);
        Registry.register(Registry.BLOCK_ENTITY_TYPE, new Identifier(ModInit.MODID, id), this.type);
    }

    private final BlockEntityType<?> type;

    @SuppressWarnings({"unchecked"})
    public <T extends BlockEntity> BlockEntityType<T> asType() {
        return (BlockEntityType<T>) type;
    }
    public static void loadClass() {
    }
}
