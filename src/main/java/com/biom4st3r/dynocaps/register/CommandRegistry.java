package com.biom4st3r.dynocaps.register;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.api.BlockContainer;
import com.biom4st3r.dynocaps.api.UnBookBan;
import com.biom4st3r.dynocaps.components.DynocapComponent;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.util.Requester;
import com.biom4st3r.dynocaps.util.ResourceHandler;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mojang.authlib.GameProfile;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.suggestion.SuggestionProvider;

import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.ChestBlockEntity;
import net.minecraft.command.CommandSource;
import net.minecraft.command.argument.IdentifierArgumentType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtIo;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.network.ServerPlayerInteractionManager;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraft.world.WorldSaveHandler;

public final class CommandRegistry {
    static File playerData = null;
    static SuggestionProvider<ServerCommandSource> SUGGESTIONS = (context, builder) -> {
        Set<Identifier> ids = Sets.newHashSet(ResourceHandler.PREFABS.keySet());
        return CommandSource.suggestIdentifiers(ids, builder);
    };
    @SuppressWarnings("deprecation")
    public static void init() {
        CommandRegistrationCallback.EVENT.register((dispatcher, isServer) -> {
            dispatcher.register((
                    CommandManager
                        .literal("dynocaps:checkSize")
                        .requires((source) -> source.getEntity() instanceof PlayerEntity)
                        .executes((context) -> {
                            PlayerEntity pe = (PlayerEntity) context.getSource().getEntity();
                            PacketByteBuf buff = new PacketByteBuf(Unpooled.buffer());

                            ItemStack stack = pe.inventory.getMainHandStack();

                            buff.writeCompoundTag(stack.toTag(new CompoundTag()));

                            String text = String.format("%.2f%s Full | %s/2097152 bytes", ((buff.writerIndex())/2097152F) * 100, "%", buff.writerIndex());
                            buff.clear();
                            context.getSource().sendFeedback(new LiteralText(text), false);
                            return 1;
                        })
                ));

            dispatcher.register(
                    CommandManager
                        .literal("rustycap")
                        .requires((source) -> source.hasPermissionLevel(2))
                        .then(CommandManager.argument("prefab", IdentifierArgumentType.identifier())
                            .suggests(SUGGESTIONS)
                            .executes((context) -> {
                                Identifier prefab = IdentifierArgumentType.getIdentifier(context, "prefab");
                                try {
                                    context.getSource().getEntity().dropStack(IDynocapComponent.fromPrefab(prefab));
                                } catch (Throwable t) {
                                    if (context.getSource().getEntity() instanceof PlayerEntity) {
                                        ((PlayerEntity) context.getSource().getEntity()).sendMessage(
                                                new LiteralText("Prefab 010 not found".replace("010", prefab.toString())), false);
                                    }
                                }
                                return 1;
                            })
                        )
            );
            dispatcher.register(
                    CommandManager.literal("unbookban")
                        .requires((s) -> s.hasPermissionLevel(2))
                        .then(CommandManager.argument("playername", StringArgumentType.string())
                            .executes(CommandRegistry::execute))
            );
            dispatcher.register(CommandManager.literal("dynocap:doit")
                        .requires((s) -> FabricLoader.getInstance().isDevelopmentEnvironment())
                        .executes((context) -> {
                            int width = 15;
                            int depth = 15;
                            int height = 15;
                            BlockContainer container = new BlockContainer(width * height * depth);
                            BlockPos.Mutable pos = new BlockPos.Mutable(0, 0, 0);
                            for (int y = 0; y < height; y++) {
                                for (int x = 0; x < width; x++) {
                                    for (int z = 0; z < depth; z++) {
                                        pos.set(x, y, z);
                                        container._rawAdd(Blocks.CHEST.getDefaultState(), pos, getNew(pos));
                                    }
                                }
                            }
                            ItemStack is = new ItemStack(ItemEnum.Dynocap);
                            DynocapComponent component = (DynocapComponent) IDynocapComponent.TYPE.get(is);
                            component.getContainer().fromTag(container.toTag(new CompoundTag()));
                            component.setColor(0xFF00FF);
                            component.setWidth(width);
                            component.setHeight(height);
                            component.setDepth(depth);
                            component.setName("doit!");
                            ResourceHandler.PREFABS.put(new Identifier("doit", "doit"), component.toTag(new CompoundTag()));
                            Entity entity = context.getSource().getEntity();
                            World world = entity.world;
                            world.spawnEntity(new ItemEntity(world, entity.getX(), entity.getY(), entity.getZ(), is));
                            System.out.println("Done");
                            return 1;
                        })
            );
        }); // No touch
    }

    private static ChestBlockEntity getNew(BlockPos pos) {
        ChestBlockEntity be = new ChestBlockEntity();
        Random random = new Random();
        List<Item> t = Registry.ITEM.stream().collect(Collectors.toCollection(() -> Lists.newArrayList()));
        for (int i = 0; i < 27; i++) {
            be.setStack(i, new ItemStack(t.get(random.nextInt(t.size()-1)), random.nextInt(60+1)));
        }
        return be;
    }

    private static int execute(CommandContext<ServerCommandSource> context) {
        if (playerData == null) {
            Field saveHandler = Stream.of(PlayerManager.class.getDeclaredFields()).filter((f) -> f.getType() == WorldSaveHandler.class).findFirst().get();
            Field playerDataFile = Stream.of(WorldSaveHandler.class.getDeclaredFields()).filter((f) -> f.getType() == File.class).findFirst().get();
            saveHandler.setAccessible(true);
            playerDataFile.setAccessible(true);
            WorldSaveHandler handler;
            try {
                handler = (WorldSaveHandler) saveHandler.get(context.getSource().getMinecraftServer().getPlayerManager());
                playerData = (File) playerDataFile.get(handler);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                return -1;
            }
        }
        String player = StringArgumentType.getString(context, "playername");
        if (Arrays.binarySearch(context.getSource().getMinecraftServer().getPlayerNames(), player) != -1) {
            context.getSource().sendFeedback(new LiteralText("Player must be offline"), true);
        }
        Optional<UUID> realUUID = Requester.getOfflinePlayerUUID(player);
        UUID offlinePlayerUuid = ServerPlayerEntity.getOfflinePlayerUuid(player);
        if (realUUID.isPresent()) {
            offlinePlayerUuid = realUUID.get();
        }

        File nbt = new File(playerData, offlinePlayerUuid.toString()+".dat");
        if (nbt.exists() && nbt.isFile()) {
            GameProfile profile = context.getSource().getMinecraftServer().getUserCache().getByUuid(offlinePlayerUuid);
            ServerPlayerEntity spe = new ServerPlayerEntity(context.getSource().getMinecraftServer(), context.getSource().getWorld(), profile, new ServerPlayerInteractionManager(context.getSource().getWorld()));
            try {
                spe.fromTag(NbtIo.readCompressed(nbt));
            } catch (IOException e) {
                context.getSource().sendError(new LiteralText("Failed to read file."));
                return -1;
            }
            PlayerInventory inventory = spe.inventory;
            PacketByteBuf buff = new PacketByteBuf(Unpooled.buffer());
            boolean changed = false;
            for (int i = 0; i < inventory.size(); i++) {
                ItemStack stack = inventory.getStack(i);
                if (stack.isEmpty()) continue;
                buff.writeCompoundTag(stack.toTag(new CompoundTag()));
                ModInit.logger.debug("Item: %s || Size: %s", stack.getItem().getTranslationKey(), buff.capacity());
                if (buff.capacity()/2 >= 2097152) {
                    changed = true;
                    UnBookBan.get(stack.getItem()).fix(spe, i, inventory, stack);
                }
                buff.clear();
            }
            if (changed) {
                CompoundTag ct = spe.toTag(new CompoundTag());
                try {
                    NbtIo.writeCompressed(ct, nbt);
                    context.getSource().sendFeedback(new LiteralText("Successfully removed oversized items. from " + player), true);
                } catch (IOException e) {
                    return -1;
                }
            } else {
                context.getSource().sendFeedback(new LiteralText("No oversized items found"), true);
            }
            return 1;
        }
        context.getSource().sendError(new LiteralText("Player save file not found. "+nbt.toPath().toString()));
        return 1;
    }
}
