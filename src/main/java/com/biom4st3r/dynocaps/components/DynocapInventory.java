package com.biom4st3r.dynocaps.components;

import java.io.IOException;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.NbtIo;
import net.minecraft.util.collection.DefaultedList;

import com.biom4st3r.dynocaps.register.ItemEnum;
import it.unimi.dsi.fastutil.io.FastByteArrayInputStream;
import it.unimi.dsi.fastutil.io.FastByteArrayOutputStream;
import it.unimi.dsi.fastutil.objects.Object2BooleanFunction;

public class DynocapInventory implements IDynoInventoryDIY {
    private DefaultedList<ItemStack> inventory = DefaultedList.ofSize(4, ItemStack.EMPTY);

    public DynocapInventory() {
    }

    @Override
    public boolean isEmpty() {
        return inventory.get(0).isEmpty() && inventory.get(1).isEmpty() && inventory.get(2).isEmpty() && inventory.get(3).isEmpty();
    }

    @Override
    public ItemStack getStack(int slot) {
        return inventory.get(slot & 3);
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        ItemStack stack = getStack(slot);
        int delta = stack.getCount() - amount;
        if (delta < 1) {
            this.setStack(slot, ItemStack.EMPTY);
            return stack;
        } else {
            stack.setCount(delta);
            return new ItemStack(stack.getItem(), amount);
        }
    }

    @Override
    public ItemStack removeStack(int slot) {
        ItemStack stack = getStack(slot);
        this.setStack(slot, ItemStack.EMPTY);
        return stack;
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        if (ItemEnum.DYNOCAPS.contains(stack.getItem()) || stack.isEmpty()) {
            this.inventory.set(slot & 3, stack.copy());
        }
    }

    @Override
    @SuppressWarnings({"unused"})
    public void markDirty() {
        String s = "I'm a dirty inventory";
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    @Override
    public void clear() {
        this.inventory.clear();
    }

    @Override
    public void copyFrom(IDynoInventoryDIY other) {
        for (int i = 0; i < other.size(); i++) {
            this.setStack(i, other.getStack(i));
        }
    }

    @Override
    public void readFromNbt(CompoundTag tag) {
        this.clear();
        ListTag items = null;
        try {
            items = (ListTag) NbtIo.readCompressed(new FastByteArrayInputStream(tag.getByteArray("items")))
                    .get("list");
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < items.size(); i++) {
            CompoundTag stackTag = (CompoundTag) items.get(i);
            setStack(i, ItemStack.fromTag(stackTag));
        }
    }

    @Override
    public void writeToNbt(CompoundTag tag) {
        if (this.isEmpty()) return;

        ListTag items = new ListTag();
        for (ItemStack stack : inventory) {
            items.add(stack.toTag(new CompoundTag()));
        }
        CompoundTag ct = new CompoundTag();

        ct.put("list", items);
        FastByteArrayOutputStream stream = new FastByteArrayOutputStream();
        try {
            NbtIo.writeCompressed(ct, stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        tag.putByteArray("items", stream.array);
    }

    static final Object2BooleanFunction<Item> isDynocap = ItemEnum.DYNOCAPS::contains;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DynocapInventory) {
            DynocapInventory that = (DynocapInventory) obj;

            for (int i = 0; i < this.size(); i++) {
                ItemStack thisStack = this.getStack(i);
                ItemStack thatStack = that.getStack(i);
                if (thisStack.isEmpty() && thatStack.isEmpty()) continue;
                else if (ItemEnum.DYNOCAPS.contains(thisStack.getItem()) && ItemEnum.DYNOCAPS.contains(thatStack.getItem())) {
                    if (IDynocapComponent.TYPE.get(thisStack).equals(IDynocapComponent.TYPE.get(thatStack))) continue;
                }
                return false;
            }
            return true;
        }
        return false;
    }
}
