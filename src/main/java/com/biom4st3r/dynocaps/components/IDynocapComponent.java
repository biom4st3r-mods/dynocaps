package com.biom4st3r.dynocaps.components;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockRenderView;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.api.BlockContainer2;
import com.biom4st3r.dynocaps.api.EntityContainer;
import com.biom4st3r.dynocaps.items.ItemTemplate;
import com.biom4st3r.dynocaps.register.ItemEnum;
import com.biom4st3r.dynocaps.rendering.AgnosticRenderContext;
import com.biom4st3r.dynocaps.rendering.DynocapRenderer;
import com.biom4st3r.dynocaps.util.ResourceHandler;
import dev.onyxstudios.cca.api.v3.component.ComponentKey;
import dev.onyxstudios.cca.api.v3.component.ComponentRegistry;
import dev.onyxstudios.cca.api.v3.component.CopyableComponent;

public interface IDynocapComponent extends CopyableComponent<IDynocapComponent> {
    ComponentKey<IDynocapComponent> TYPE = ComponentRegistry.getOrCreate(new Identifier(ModInit.MODID, "dynocap"), IDynocapComponent.class);

    static ItemStack fromPrefab(Identifier id) {
        ItemStack stack = new ItemStack(ItemEnum.AncientCap);
        if (ResourceHandler.PREFABS.containsKey(id)) {
            TYPE.get(stack).readFromNbt(ResourceHandler.PREFABS.get(id));
            return stack;
        }
        throw new RuntimeException("Invalid prefab id");
    }

    enum Result {
        SUCCESS(""),
        PREVENT_OVERRIDES("dialog.dynocaps.PREVENT_OVERRIDES"),
        PROHIBIITED_BLOCK("dialog.dynocaps.PROHIBIITED_BLOCK"),
        GOLM("dialog.dynocaps.golm_protection"),
        FLAN("dialog.dynocaps.flan_protection"),
        MAX_HEIGHT("dialog.dynocaps.maxheight"),
        MIN_HEIGHT("dialog.dynocaps.minheight"),
        ADVENTURE_MODE("dialog.dynocaps.adventure_mode");

        public TranslatableText response;
        Result(String text) {
            response = new TranslatableText(text);
        }

        public boolean isSuccess() {
            return this == SUCCESS;
        }

        public <T> TypedActionResult<T> finish(PlayerEntity player, T obj) {
            if (!response.getKey().isEmpty()) player.sendSystemMessage(response, Util.NIL_UUID);
            return new TypedActionResult<T>(this.resultToResult(), obj);
        }
        ActionResult resultToResult() {
            return this.ordinal() == 0 ? ActionResult.SUCCESS : ActionResult.FAIL;
        }
    }

    ItemTemplate getTemplate();

    boolean shouldPlaceAir();
    void setShouldPlaceAir(boolean b);

    int getWidth();

    int getHeight();

    int getDepth();

    void setWidth(int i);

    void setHeight(int i);

    void setDepth(int i);

    Result captureBlocks(ServerWorld world, BlockPos bottomLeft, PlayerEntity pe, boolean destroy);

    Result unprotectedCaptureBlocks(ServerWorld world, BlockPos bottomLeft, PlayerEntity pe, boolean destory);

    BlockContainer2 getContainer();
    EntityContainer getEntities();

    Result releaseBlocks(ServerWorld world, BlockPos pos, PlayerEntity pe);

    Result unprotectedReleaseBlocks(ServerWorld world, BlockPos pos, PlayerEntity pe);

    boolean isFilled();

    int getColor();

    void setColor(int i);

    boolean getPlaceAir();
    void setPlaceAir(boolean bl);

    String getName();
    void setName(String s);

    @Environment(EnvType.CLIENT)
    DynocapRenderer getRenderer();
    @Environment(EnvType.CLIENT)
    AgnosticRenderContext getCache();
    @Environment(EnvType.CLIENT)
    default BlockRenderView getView() {
        return this.getContainer().asBlockRenderView();
    }
}
