package com.biom4st3r.dynocaps.blocks;

import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.register.BlockEntityEnum;
import com.biom4st3r.dynocaps.register.ItemEnum;

import net.fabricmc.fabric.api.block.entity.BlockEntityClientSerializable;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

public class DynocapDisplay extends Block implements BlockEntityProvider {
    public DynocapDisplay(Settings settings) {
        super(settings.nonOpaque().noCollision());
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
        ItemStack stack = player.getStackInHand(hand);
        if (ItemEnum.DYNOCAPS.contains(stack.getItem())) {
            if (IDynocapComponent.TYPE.get(stack).isFilled()) {
                BlockEntity be = world.getBlockEntity(pos);
                if (be.getType() == BlockEntityEnum.DISPLAY.asType()) {
                    DynocapBlockEntity dyno = (DynocapBlockEntity) be;
                    if (dyno.dynocap.isEmpty()) {
                        dyno.setDynocap(stack);
                        return ActionResult.CONSUME;
                    } else {
                        player.giveItemStack(dyno.dynocap);
                        dyno.dynocap = ItemStack.EMPTY;
                        return ActionResult.SUCCESS;
                    }
                }
            }
        }
        return ActionResult.PASS;
    }

    @Override
    public BlockEntity createBlockEntity(BlockView world) {
        return new DynocapBlockEntity();
    }

    public static class DynocapBlockEntity extends BlockEntity implements BlockEntityClientSerializable {
        ItemStack dynocap = ItemStack.EMPTY;

        public DynocapBlockEntity() {
            super(BlockEntityEnum.DISPLAY.asType());
        }

        public ItemStack getDynocap() {
            return dynocap;
        }

        public void setDynocap(ItemStack is) {
            this.dynocap = is.copy();
        }

        @Override
        public void fromClientTag(CompoundTag tag) {
            if (!tag.isEmpty()) {
                this.dynocap = ItemStack.fromTag(tag);
            }
        }

        @Override
        public CompoundTag toClientTag(CompoundTag tag) {
            if (dynocap.isEmpty()) return new CompoundTag();
            else return dynocap.toTag(new CompoundTag());
        }

        @Override
        public CompoundTag toTag(CompoundTag tag) {
            if (!dynocap.isEmpty()) {
                tag.put("item", dynocap.toTag(new CompoundTag()));
            }
            return super.toTag(tag);
        }

        @Override
        public void fromTag(BlockState state, CompoundTag tag) {
            if (tag.contains("item")) {
                this.dynocap = ItemStack.fromTag(tag.getCompound("item"));
            }
            super.fromTag(state, tag);
        }
    }
}
