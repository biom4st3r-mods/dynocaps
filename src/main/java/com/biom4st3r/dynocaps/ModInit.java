package com.biom4st3r.dynocaps;

import java.io.IOException;
import java.util.Random;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;

import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.items.ItemTemplate;
import com.biom4st3r.dynocaps.register.BlockEntityEnum;
import com.biom4st3r.dynocaps.register.BlockEnum;
import com.biom4st3r.dynocaps.register.CommandRegistry;
import com.biom4st3r.dynocaps.register.DispenserBehaviors;
import com.biom4st3r.dynocaps.register.DynocapEntryItemEntry;
import com.biom4st3r.dynocaps.register.ItemEnum;
import com.biom4st3r.dynocaps.register.Packets;
import com.biom4st3r.dynocaps.register.ScreenHandlerEnum;
import com.biom4st3r.dynocaps.register.TestCommand;
import com.biom4st3r.dynocaps.util.ResourceHandler;
import io.github.cottonmc.cotton.config.ConfigManager;

public class ModInit implements ModInitializer {
    public static final String MODID = "dynocaps";
    public static final BioLogger logger = new BioLogger(MODID);

    public static Config config = ConfigManager.loadConfig(Config.class);

    public static final ItemGroup DYNO_GROUP = FabricItemGroupBuilder.create(new Identifier(ModInit.MODID, "dynocap_tab")).icon(() -> {
        ItemStack is = new ItemStack(ItemEnum.Dynocap);
        IDynocapComponent.TYPE.get(is).setColor(new Random().nextInt());
        return is;
    }).build();

    public ModInit() {
    }

    @Override
    public void onInitialize() {
        ResourceHandler.loadDefaultDynocap();
        // #region Load Classes
        DynocapEntryItemEntry.init();
        ItemEnum.loadClass();
        Packets.SERVER.init();
        DispenserBehaviors.init();
        CommandRegistry.init();
        TestCommand.init();
        BlockEnum.loadClass();
        BlockEntityEnum.loadClass();
        ScreenHandlerEnum.loadClass();
        // #endregion

        ServerLifecycleEvents.SERVER_STARTED.register((server) -> {
            ItemTemplate.resetDefaultCap();
        });

        try {
            ResourceHandler.init();
        } catch (IOException e) {
            logger.error("\n\nResourceHandler failed!\n\n");
            e.printStackTrace();
        }
    }

    static {
        CCA_Hax.init();
    }
}
